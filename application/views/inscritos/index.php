<script>
    var listaInscritos = null;
    jQuery(document).ready(function (e) {
        listaInscritos = jQuery('#table_inscritos').dataTable({
            "sPaginationType": "full_numbers",
            //'sAjaxSource': '{base_url}inscritos/getList/{id_evento_atual}',
            "iDisplayLength": 50,
        });
        jQuery('#search_atleta').focus();
        //jQuery('#table_inscritos_filter span').text('Pesquisa rápida');
        setInterval(adicionarCliques, 500);

        // Adicionar focus no campo de pesquisa
        //jQuery('#search_atleta').focus();
    });

    function dataTableLoad() {
        if (jQuery('#loadPage').length == 0) {
            jQuery('#table_inscritos_filter input').val('').trigger('keyup');

            // Acertar Load
            var tamanhoPagina = jQuery(document).height();
            jQuery('body').append('<div id="loadPage"><span>&nbsp;</span></div>');
            jQuery('#loadPage').height(tamanhoPagina);

            var sem_numero = 0;
            if (jQuery('.sem_numero').is(':checked'))
                sem_numero = 1;

            // Carregar
            jQuery('#table_inscritos_filter input').val('').trigger('keyup');
            listaInscritos.fnReloadAjax('{base_url}inscritos/getList/{id_evento_atual}?search_tipo=' + jQuery('#search_tipo').val() + '&search_atleta=' + jQuery('#search_atleta').val() + '&sem_numero=' + sem_numero);
        }
    }

    function dataTableRecarregado() {
        jQuery('#loadPage').remove();
    }

    function adicionarCliques() {
        // 8 busca nome balcão
        if (8 == jQuery('#search_tipo').val()) {
            var urlBase = '{base_url}inscritos/balcao/{id_evento_atual}/' + jQuery('#search_atleta').val() + '/';
        } else {
            var urlBase = '{base_url}inscritos/detalhe/{id_evento_atual}/';
        }

        jQuery('#table_inscritos tbody tr').each(function (index, element) {
            if (!jQuery(this).hasClass('clickOK') && jQuery(this).attr('id')) {

                var dados = jQuery(this).attr('id').split("_");
                var url = urlBase + dados[1];
                var urlUsuario = '{base_url}inscritos/atleta/' + dados[2] + '/' + dados[2];

                jQuery(this).find('td').eq(0).html('<a href="' + url + '">' + jQuery(this).find('td').eq(0).text() + '</a>');
                jQuery(this).find('td').eq(1).html('<a href="' + url + '">' + jQuery(this).find('td').eq(1).text() + '</a>');
                jQuery(this).addClass('clickOK');
            }
        });

        triggerRetiradoSim();
    }

    jQuery('#form_pedido').live('submit', function () {

        var pedido = 0;
        var validar = 1;
        jQuery('#form_pedido input').each(function (index, element) {
            if (jQuery(this).attr('type') == 'checkbox') {
                if (jQuery(this).is(':checked')) {
                    // 8 = filtro tipo balcão
                    if ((pedido > 0 && pedido != jQuery(this).data('pedido')) && jQuery("#search_tipo").val() != 8) {
                        alert("Você não pode fazer retirada de Pedidos diferentes!");
                        validar = 0;
                    }
                    pedido = jQuery(this).data('pedido');
                    comprador = jQuery(this).data('comprador');
                    nome_comprador = jQuery(this).data('nome_comprador');
                    telefone_comprador = jQuery(this).data('telefone_comprador');
                }
            }
        });

<?php if (strlen($ID) > 10) { ?>
            document.location = '{base_url}inscritos';
            return false;
<?php } ?>

        if (validar == 1) {
            // Abrir LightBox
            jQuery.fancybox({
                maxWidth: 800,
                maxHeight: 600,
                fitToView: false,
                width: '70%',
                height: '70%',
                autoSize: false,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none',
                content: jQuery('#retirada').html()
            });

            if (pedido > 0) {
                var url = jQuery('.fancybox-outer .form_retirada').attr('action');
                url = url + pedido;
                jQuery('.fancybox-outer .form_retirada').attr('action', url);
                jQuery('.fancybox-outer .form_retirada .eh_comprador').val(comprador);
                //jQuery('.fancybox-outer .form_retirada .nome_retirado').val(nome_comprador);
                jQuery('.fancybox-outer .form_retirada .nome_retirado').attr('data-inicial', nome_comprador);
                jQuery('.fancybox-outer .form_retirada .nome_retirado_span').text(nome_comprador);
                jQuery('.fancybox-outer .form_retirada .telefone_retirado').val(telefone_comprador);
                jQuery('.fancybox-outer .form_retirada .telefone_retirado').attr('data-inicial', telefone_comprador);
                jQuery('.fancybox-outer .form_retirada .telefone_retirado_span').text(telefone_comprador);
            }

            // Adicionar Click do EhComprador
            jQuery('.fancybox-outer .form_retirada .eh_comprador').click(function () {
                if (jQuery(this).is(':checked')) {
                    jQuery('.fancybox-outer .form_retirada').removeClass('nao_comprador');
                    jQuery('.fancybox-outer .form_retirada .nome_retirado').val(jQuery('.fancybox-outer .form_retirada .nome_retirado').attr('data-inicial'));
                    jQuery('.fancybox-outer .form_retirada .telefone_retirado').val(jQuery('.fancybox-outer .form_retirada .telefone_retirado').attr('data-inicial'));
                } else {
                    jQuery('.telefone_retirado').mask("(99) 99999-9999");
                    document.querySelector("body > div.fancybox-overlay.fancybox-overlay-fixed > div > div > div > div > form > table > tbody > tr:nth-child(1) > td:nth-child(2) > input").setAttribute('required', 'required');
                    document.querySelector("body > div.fancybox-overlay.fancybox-overlay-fixed > div > div > div > div > form > table > tbody > tr:nth-child(2) > td:nth-child(2) > input").setAttribute('required', 'required');
                    jQuery('.fancybox-outer .form_retirada').addClass('nao_comprador');
                    jQuery('.fancybox-outer .form_retirada .nome_retirado').val('');
                    jQuery('.fancybox-outer .form_retirada .telefone_retirado').val('');
                }
            });

            // Adicionar Inputs ao LightBox
            jQuery('#form_pedido input').each(function (index, element) {
                if (jQuery(this).attr('type') == 'checkbox') {
                    if (jQuery(this).is(':checked')) {
                        var html = '<input type="hidden" name="' + jQuery(this).attr('name') + '"';
                        html += (jQuery(this).is(':checked') ? ' value="1"' : ' value="0"') + '>';

                        jQuery('.fancybox-outer .form_retirada .inputs_hidden').append(html);
                    }
                }
            });
        }

        return false;
    });


    //  jQuery('#btnNovaInscricao').click(function () {
    jQuery('#novaInscricao').live('submit', function () {
        var valida = 1;

        dados = jQuery(this).serialize();
        jQuery.ajax({
            type: "POST",
            url: '{base_url}inscritos/nova?' + dados,
            dataType: 'html',
            cache: false
        }).done(function (data) {
            retorno = JSON.parse(data);

            alert(retorno.info);

            if (retorno.status == 'ok') {
                window.location.href = '{base_url}' + retorno.url;
            }
        });

        return false;
    });

</script>
<style>
    .existente{
        background-color: #E18B7C;
    }    
    .assinante{
        background: #99a1ff;
    }
    .cortesia{
        background: #b9f1df;
    }
    .roxo{
        background: #c09ac0;
    }
    #form_pedido label{
        float: none !important;
    }
    #form_pedido input[type=text]{
        width: auto !important;
    }
    .dataTables_filter {
        display: none; 
    }


    .item_pelotao{
        border-radius: 50%;
        border: 1px solid #000;
        padding: 4px 10px;
        margin-left: 10px;
    }

    .pelotao_quenia{
        background: black;
    }
    .pelotao_azul{
        background: blue;
    }

    .pelotao_verde{
        background: green;
    }

    .pelotao_branco{
        background: white;
    }

    #event_balcao{ display: none;}
</style>
<div class="pageheader">
    <h1 class="pagetitle">Inscritos</h1>
    <span class="pagedesc">Lista de participantes Inscritos evento {evento_nome}.</span>

    <ul class="hornav">
        <li class="current"><a href="#event_new">Inscritos</a></li>
        <li><a href="#event_balcao">Balcão</a></li>
        <?php if ($NIVEL_ADMIN) { ?><li><a href="#event_old">Nova Inscrição</a></li><?php } ?>
        <?php if ($NIVEL_ADMIN) { ?><li><a href="#pedido_existente">Pedido Existente</a></li><?php } ?>
    </ul>
</div><!--pageheader-->

<div id="contentwrapper" class="contentwrapper">

    <div id="event_new" class="subcontent">
        <div class="contenttitle2">
            <h3>Inscrito</h3>
        </div>
        <form action="#" onsubmit="dataTableLoad();
                return false;">
            <div class="overviewhead" id="table_inscritos_filtro">
                <strong>Buscar por</strong>: &nbsp;&nbsp;
                <div class="overviewselect" style="float: none; display: inline-block;">
                    <select id="search_tipo" name="search_tipo">
                        <option value="0">Protocolo</option>
                        <?php if ($NIVEL_ADMIN == 1): ?>
                            <option value="7">Nome Atleta</option>
                        <?php endif; ?>                            
                        <option value="1">Documento</option>
                        <option value="2">Nº Peito</option>

                        <?php /*  ?>
                          <option value="3">E-mail</option>
                          <option value="5">Telefone</option>
                          <option value="4">Celular</option>
                          <option value="6">Equipe</option>
                          <option value="8">Nome Balcão</option>
                          <?php */ ?>

                    </select>
                </div>
                <input type="text" class="smallinput" name="search_atleta" id="search_atleta" style="height: 10px; margin-top: -1px;" onkeyup="if (event.keyCode == 13)
                            return false;
                        var digitado = String(jQuery('#search_atleta').val());
                        if (digitado.substr(0, 1) == '0')
                            digitado = digitado.substr(1);
                        jQuery('#table_inscritos_filter input').val(digitado).trigger('keyup');">
                <a href="{base_url}inscritos" >Limpar</a>
                <label for="sem_numero" style="margin-left: 20px;">
                    <input type="checkbox" name="sem_numero" class="sem_numero" style="float: left;"> sem número de peito
                </label>
                <input type="submit" value="Buscar" style="float: right;">
            </div>
        </form>
        <script> jQuery('#overviewselect, input:checkbox').uniform();</script>

        <form class="stdform" id="form_pedido">
            <table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="table_inscritos">
                <thead>
                    <tr>
                        <th class="head0" width="100">Protocolo</th>
                        <th class="head1">Atleta</th>
                        <th class="head0" width="50">Pelotão</th>
                        <th class="head0" width="50">N. Peito</th>
                        <th class="head1" width="50">Status</th>
                        <th class="head0" width="100">Modalidade</th>
                        <th class="head1" width="150">Kit</th>
                        <th class="head0" width="100">Camiseta</th>
                        <th class="head1" width="50">Brinde</th>
                        <th class="head0">Documento</th>
                        <th class="head1">Produtos<br /></th>
                        <th class="head1">Personalização<br /></th>
                        <th class="head0" width="70">Retirar Kit</th>
                        <th class="head0">Data</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th class="head0" width="100">Protocolo</th>
                        <th class="head1">Atleta</th>
                        <th class="head0" width="50">Pelotão</th>
                        <th class="head0" width="50">N. Peito</th>
                        <th class="head1" width="50">Status</th>
                        <th class="head0" width="100">Modalidade</th>
                        <th class="head1" width="150">Kit</th>
                        <th class="head0" width="100">Camiseta</th>
                        <th class="head1" width="50">Brinde</th>
                        <th class="head0">Documento</th>
                        <th class="head1">Produtos<br /></th>
                        <th class="head1">Personalização<br /></th>
                        <th class="head0" width="70">Retirar Kit</th>
                        <th class="head0">Data</th>
                    </tr>
                </tfoot>
                <tbody>&nbsp;</tbody>
            </table>
            <a href="#" style="color: blue;" onclick="jQuery('input[name^=retirada_incricoes]').prop('checked', true); return false;">Marcar todos</a> |
            <a href="#" style="color: blue;" onclick="jQuery('input[name^=retirada_incricoes]').prop('checked', false); return false;">Desmarcar todos</a>

            <input type="submit" class="submit radius2" value="Retirar" style="float: right;">
        </form>



    </div><!--contentwrapper-->


    <div id="event_balcao" class="subcontent">
        <div class="contenttitle2">
            <h3>Balcões</h3>
        </div>

        <table cellpadding="0" cellspacing="0" border="0" class="stdtable">
            <thead>
                <tr>
                    <th class="head0">Nome</th>
                    <th class="head1">Quantidade</th>
                    <th class="head0">Ações</th>
                </tr>
            </thead>
            <tbody>
                {LISTA_BALCAO}
                <tr>
                    <td>
                        {nome_balcao}
                    </td>
                    <td>
                        {total}
                    </td>
                    <td>
                        <a href="<?php echo $base_url; ?>{url_balcao}">

                            Lista Participantes
                        </a>                        
                    </td>
                </tr>
                {/LISTA_BALCAO}
            </tbody>
        </table>
    </div>



    <?php if ($NIVEL_ADMIN) { ?>
        <div id="event_old" class="subcontent">
            <form id="novaInscricao" class="stdform" method="post">
                <div id="wizard" class="wizard">
                    <div class="stepContainer">
                        <div id="wiz1step1" class="formwiz content">
                            <h4>Dados atleta</h4>
                            <p>
                                <label>E-mail</label>
                                <span class="field"><input type="text" name="email" id="email" class="longinput email" required="required" /></span>
                            </p>
                            <p>
                                <label>Nome</label>
                                <span class="field"><input type="text" name="nome_completo" id="nome_completo" class="longinput" required="required" /></span>
                            </p>
                            <p>
                                <label>Tipo documento</label>
                                <span class="field">
                                    <select name="h_tipo_cpf" id="h_tipo_cpf" class="h_tipo_cpf">
                                        <option value="1" data-mask="999.999.999-99">Documento obrigatório para maiores de 16 anos</option>
                                        <option value="2" data-mask="999.999.999-99">Documento do responsavel obrigatório para menores de 16 anos</option>
                                        <option value="4" data-mask="99999999999">Estrangeiros (Passaporte)</option>
                                    </select>
                                </span>
                            </p>
                            <p>
                                <label>Documento</label>
                                <span class="field"><input type="text" name="v_nr_documento" id="v_nr_documento" class="longinput v_nr_documento" required="required" /></span>
                            </p>
                            <p>
                                <label>Nascimento</label>
                                <span class="field">
                                    <input type="text" value="" name="nascimento1" id="nasc4" class="tam2 valid" style="width: 30px;" maxlength="2" onkeyup="if (jQuery(this).val().length >= 2)
                                                jQuery(this).next().focus();" required="required">
                                    <input type="text" value="" name="nascimento2" id="nasc5" class="tam2 valid" style="width: 30px;" maxlength="2" onkeyup="if (jQuery(this).val().length >= 2)
                                                jQuery(this).next().focus();" required="required">
                                    <input type="text" value="" name="nascimento3" id="nasc6" class="tam2 valid" style="width: 50px;" maxlength="4" required="required">
                                </span>
                            </p>
                            <p>
                                <label>Sexo</label>
                                <span class="field">
                                    <select name="genero" id="genero" required="required">
                                        <option value="">Selecione</option>
                                        <option value="M">Masculino</option>
                                        <option value="F">Feminino</option>
                                    </select>
                                </span>
                            </p>
                            <p>
                                <label>Estado</label>
                                <span class="field">
                                    <select name="id_estado" id="id_estado" onchange="getCidades(this.value);" required="required" class="id_estado">
                                        <option value="" label="Seleccione">Seleccione</option>
                                        <option value="1" label="Acre">Acre</option>
                                        <option value="2" label="Alagoas">Alagoas</option>
                                        <option value="3" label="Amazonas">Amazonas</option>
                                        <option value="4" label="Amapá">Amapá</option>
                                        <option value="5" label="Bahia">Bahia</option>
                                        <option value="6" label="Ceará">Ceará</option>
                                        <option value="7" label="Distrito Federal">Distrito Federal</option>
                                        <option value="8" label="Espírito Santo">Espírito Santo</option>
                                        <option value="9" label="Goiás">Goiás</option>
                                        <option value="10" label="Maranhão">Maranhão</option>
                                        <option value="11" label="Minas Gerais">Minas Gerais</option>
                                        <option value="12" label="Mato Grosso do Sul">Mato Grosso do Sul</option>
                                        <option value="13" label="Mato Grosso">Mato Grosso</option>
                                        <option value="14" label="Pará">Pará</option>
                                        <option value="15" label="Paraíba">Paraíba</option>
                                        <option value="16" label="Pernambuco">Pernambuco</option>
                                        <option value="17" label="Piauí">Piauí</option>
                                        <option value="18" label="Paraná">Paraná</option>
                                        <option value="19" label="Rio de Janeiro">Rio de Janeiro</option>
                                        <option value="20" label="Rio Grande do Norte">Rio Grande do Norte</option>
                                        <option value="21" label="Rondônia">Rondônia</option>
                                        <option value="22" label="Roraima">Roraima</option>
                                        <option value="23" label="Rio Grande do Sul">Rio Grande do Sul</option>
                                        <option value="24" label="Santa Catarina">Santa Catarina</option>
                                        <option value="25" label="Sergipe">Sergipe</option>
                                        <option value="26" label="São Paulo">São Paulo</option>
                                        <option value="27" label="Tocantins">Tocantins</option>
                                    </select>
                                </span>
                            </p>
                            <p>
                                <label>Cidade</label>
                                <span class="field lista_cidade" id="lista_cidade">
                                    <select name="id_cidade" id="id_cidade" required="required">
                                        <option value="" label="Seleccione">Selecione</option>
                                    </select>
                                </span>
                            </p>
                            <p>
                                <label>Telefone</label>
                                <span class="field"><input type="text" name="telefone" id="telefone" class="longinput telefone" required="required" /></span>
                            </p>
                            <p>
                                <label>Celular</label>
                                <span class="field"><input type="text" name="celular" id="celular" class="longinput celular" required="required" /></span>
                            </p>
                        </div><!--#wiz1step1-->
                    </div>
                    <div class="stepContainer">
                        <div id="wiz1step2" class="formwiz content">
                            <h4>Dados Inscrição</h4>
                            <p>
                                <label>Modalidade</label>
                                <span class="field">
                                    <select name="modalidade" id="modalidade" class="modalidade" required="required">
                                        <option value="">Selecione</option>
                                        {MODALIDADE}<option value="{cod_modalidade}">{nome}</option>{/MODALIDADE}
                                    </select>
                                </span>
                            </p>
                            <p>
                                <label>Categoria</label>
                                <span class="field">
                                    <select name="categoria" id="categoria" class="categoria" required="required">
                                        <option value="">Selecione</option>
                                        {CATEGORIA}<option value="{cod_categoria}" data-modalidade="{id_modalidade}" style="display: none;">{nome}</option>{/CATEGORIA}
                                    </select>
                                </span>
                            </p>
                            <p>
                                <label>Camiseta</label>
                                <span class="field">
                                    <select name="camiseta" id="camiseta">
                                        <option value="">Selecione</option>
                                        {CAMISETA}<option value="{cod_tamanho_camiseta}">{nome}</option>{/CAMISETA}
                                    </select>
                                </span>
                            </p>
                            <p>
                                <label>Número Peito</label>
                                <span class="field"><input type="text" name="nm_peito" style="width:40%;" id="nm_peito" class="mediuminput nm_peito" required="required" /></span>
                            </p>
                            <p>
                                <label>Forma de pago</label>
                                <span class="field">
                                    <select name="forma_pagamento" id="forma_pagamento" required="required">
                                        <option value="">Selecione</option>
                                        <option value="1">Dinheiro</option>
                                        <option value="2">Cartão</option>
                                        <option value="3">Cortesia</option>
                                    </select>
                                </span>
                            </p>

                            <p>
                                <label>Valor Inscrição</label>
                                <span class="field"><input type="text" name="nm_preco" style="width:40%;" id="nm_preco" class="mediuminput nm_preco" required="required" /></span>
                            </p>
                        </div><!--#wiz1step2-->
                    </div>
                    <div class="actionBar">                        
                        <input type="submit" class="buttonFinish" value="Salvar" style="float: right"/>
                    </div>
                </div><!--#wizard-->
            </form>
        </div>
    <?php } ?>

    <?php if ($NIVEL_ADMIN) { ?>
        <div id="pedido_existente" class="subcontent" style="display:none;">
            <form class="stdform" method="post" action="{base_url}inscritos/existente">
                <div id="wizard" class="wizard">
                    <div class="stepContainer">
                        <div id="wiz1step1" class="formwiz content">
                            <h4>Dados do atleta/pedido</h4>
                            <p>
                                <label>Pedido</label>
                                <span class="field"><input type="text" name="id_pedido" id="id_pedido" class="longinput" required="required" /></span>
                            </p>
                            <p>
                                <label>E-mail</label>
                                <span class="field"><input type="text" name="email" id="email" class="longinput email" required="required" /></span>
                            </p>
                            <p>
                                <label>Nome</label>
                                <span class="field"><input type="text" name="nome_completo" id="nome_completo" class="longinput" required="required" /></span>
                            </p>
                            <p>
                                <label>Tipo documento</label>
                                <span class="field">
                                    <select name="h_tipo_cpf" id="h_tipo_cpf" class="h_tipo_cpf">
                                        <option value="1" data-mask="999.999.999-99">Documento obrigatório para maiores de 16 anos</option>
                                        <option value="2" data-mask="999.999.999-99">Documento do responsavel obrigatório para menores de 16 anos</option>
                                        <option value="4" data-mask="99999999999">Estrangeiros (Passaporte)</option>
                                    </select>
                                </span>
                            </p>
                            <p>
                                <label>Documento</label>
                                <span class="field"><input type="text" name="v_nr_documento" id="v_nr_documento" class="longinput v_nr_documento" required="required" /></span>
                            </p>
                            <p>
                                <label>Nascimento</label>
                                <span class="field">
                                    <input type="text" value="" name="nascimento1" id="nasc4" class="tam2 valid" style="width: 20px;" maxlength="2" onkeyup="if (jQuery(this).val().length >= 2)
                                                jQuery(this).next().focus();" required="required">
                                    <input type="text" value="" name="nascimento2" id="nasc5" class="tam2 valid" style="width: 20px;" maxlength="2" onkeyup="if (jQuery(this).val().length >= 2)
                                                jQuery(this).next().focus();" required="required">
                                    <input type="text" value="" name="nascimento3" id="nasc6" class="tam2 valid" style="width: 40px;" maxlength="4" required="required">
                                </span>
                            </p>
                            <p>
                                <label>Sexo</label>
                                <span class="field">
                                    <select name="genero" id="genero" required="required">
                                        <option value="">Selecione</option>
                                        <option value="M">Masculino</option>
                                        <option value="F">Feminino</option>
                                    </select>
                                </span>
                            </p>
                            <p>
                                <label>Estado</label>
                                <span class="field">
                                    <select name="id_estado" id="id_estado" onchange="getCidades(this.value);" required="required" class="id_estado">
                                        <option value="" label="Seleccione">Seleccione</option>
                                        <option value="1" label="Acre">Acre</option>
                                        <option value="2" label="Alagoas">Alagoas</option>
                                        <option value="3" label="Amazonas">Amazonas</option>
                                        <option value="4" label="Amapá">Amapá</option>
                                        <option value="5" label="Bahia">Bahia</option>
                                        <option value="6" label="Ceará">Ceará</option>
                                        <option value="7" label="Distrito Federal">Distrito Federal</option>
                                        <option value="8" label="Espírito Santo">Espírito Santo</option>
                                        <option value="9" label="Goiás">Goiás</option>
                                        <option value="10" label="Maranhão">Maranhão</option>
                                        <option value="11" label="Minas Gerais">Minas Gerais</option>
                                        <option value="12" label="Mato Grosso do Sul">Mato Grosso do Sul</option>
                                        <option value="13" label="Mato Grosso">Mato Grosso</option>
                                        <option value="14" label="Pará">Pará</option>
                                        <option value="15" label="Paraíba">Paraíba</option>
                                        <option value="16" label="Pernambuco">Pernambuco</option>
                                        <option value="17" label="Piauí">Piauí</option>
                                        <option value="18" label="Paraná">Paraná</option>
                                        <option value="19" label="Rio de Janeiro">Rio de Janeiro</option>
                                        <option value="20" label="Rio Grande do Norte">Rio Grande do Norte</option>
                                        <option value="21" label="Rondônia">Rondônia</option>
                                        <option value="22" label="Roraima">Roraima</option>
                                        <option value="23" label="Rio Grande do Sul">Rio Grande do Sul</option>
                                        <option value="24" label="Santa Catarina">Santa Catarina</option>
                                        <option value="25" label="Sergipe">Sergipe</option>
                                        <option value="26" label="São Paulo">São Paulo</option>
                                        <option value="27" label="Tocantins">Tocantins</option>
                                    </select>
                                </span>
                            </p>
                            <p>
                                <label>Cidade</label>
                                <span class="field lista_cidade" id="lista_cidade" >
                                    <select name="id_cidade" id="id_cidade" required="required">
                                        <option value="" label="Seleccione">Selecione</option>
                                    </select>
                                </span>
                            </p>
                            <p>
                                <label>Telefone</label>
                                <span class="field"><input type="text" name="telefone" id="telefone" class="longinput telefone" required="required" /></span>
                            </p>
                            <p>
                                <label>Celular</label>
                                <span class="field"><input type="text" name="celular" id="celular" class="longinput celular" required="required" /></span>
                            </p>
                        </div><!--#wiz1step1-->
                    </div>
                    <div class="stepContainer">
                        <div id="wiz1step2" class="formwiz content">
                            <h4>Dados Inscrição</h4>
                            <p>
                                <label>Modalidade</label>
                                <span class="field">
                                    <select name="modalidade" id="modalidade" class="modalidade">
                                        <option value="">Selecione</option>
                                        {MODALIDADE}<option value="{cod_modalidade}">{nome}</option>{/MODALIDADE}
                                    </select>
                                </span>
                            </p>
                            <p>
                                <label>Categoria</label>
                                <span class="field">
                                    <select name="categoria" id="categoria" class="categoria">
                                        <option value="">Selecione</option>
                                        {CATEGORIA}<option value="{cod_categoria}" data-modalidade="{id_modalidade}" style="display: none;">{nome}</option>{/CATEGORIA}
                                    </select>
                                </span>
                            </p>
                            <p>
                                <label>Camiseta</label>
                                <span class="field">
                                    <select name="camiseta" id="camiseta" required="required">
                                        <option value="">Selecione</option>
                                        {CAMISETA}<option value="{cod_tamanho_camiseta}">{nome}</option>{/CAMISETA}
                                    </select>
                                </span>
                            </p>
                            <p>
                                <label>Número Peito</label>
                                <span class="field"><input type="text" name="nm_peito" style="width:40%;" id="nm_peito" class="mediuminput nm_peito" required="required" /></span>
                            </p>                            
                        </div><!--#wiz1step2-->
                    </div>

                    <div class="actionBar">
                        <input type="submit" class="buttonFinish" value="Salvar" style="float: right;" />
                    </div>
                </div><!--#wizard-->
            </form>
        </div>
    <?php } ?>

    <div id="retirada">
        <div class="contenttitle2">
            <h3>Retirada do Kit</h3>
        </div>
        <br clear="all" />
        <form action="{base_url}inscritos/retirado/{ID_EVENTO}/" method="post" class="form_retirada">
            <label><input type="checkbox" name="eh_comprador" class="eh_comprador" checked="checked" /> O comprador que está retirando o Kit.</label>
            <br clear="all" />
            <table cellpadding="0" cellspacing="0" class="table invoicefor" style="height: 100%;">
                <tbody>
                    <tr>
                        <td width="20%" style="vertical-align: middle;"><strong>Nome:</strong></td>
                        <td width="80%"><span class="nome_retirado_span"></span><input type="text" name="nome_retirado" class="nome_retirado" value="" data-inicial="" /></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;"><strong>Telefone:</strong></td>
                        <td><span class="telefone_retirado_span telefone"></span><input type="text" name="telefone_retirado" class="telefone_retirado" value="" data-inicial="" /></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;"><strong>Observação:</strong></td>
                        <td><textarea name="obs" required></textarea></td>
                    </tr>
                </tbody>
            </table>
            <div class="inputs_hidden"></div>
            <br clear="all" />
            <a href="<?php echo $base_url; ?>inscritos/" class="buttonFinish" style="float: left;">Voltar</a>
            <input type="submit" class="submit radius2" value="Retirar" style="float: right;">
        </form>
    </div>

    <br clear="all" />

</div><!-- centercontent -->
<script type="text/javascript">
    function getCidades(id_estado) {
        jQuery.get(base_url + 'inscritos/get_cidade/' + id_estado, function (data) {
            jQuery(".lista_cidade").html(data);
        });
    }

    jQuery(document).ready(function (e) {
        jQuery('.modalidade').change(function () {
            jQuery('.categoria option').css('display', 'none');
            jQuery('.categoria option[data-modalidade="' + jQuery(this).val() + '"]').css('display', 'block');
        });
    });
</script>
<script type="text/javascript" src="{base_url}assets/js/plugins/jquery.mask.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function (e) {
        jQuery('.telefone').mask("(99) 99999-9999");
        jQuery('.celular').mask("(99) 99999-9999");

        jQuery('.h_tipo_cpf').change(addMaskDocument).blur(addMaskDocument);
        addMaskDocument();

        jQuery('.email').blur(function () {
            if (!verificaEMAIL(jQuery(this).val())) {
                jQuery(this).val('');
                alert('E-mail invalido.');
                return false;
            }
        });

        checkNmPeitoCadastrado({id_evento_atual});
        checkPedidoCadastrado({id_evento_atual});

        jQuery('.nm_peito').keyup(function () {
            this.value = this.value.replace(/[^0-9]/g, '');
        });

        jQuery('.nm_preco').keyup(function () {
            this.value = this.value.replace(/[^0-9\.]/g, '');
        });
    });

    function addMaskDocument() {
        jQuery('.v_nr_documento').mask(jQuery('.h_tipo_cpf option:selected').attr('data-mask'));
    }

    function validarCPF(cpf) {
        cpf = cpf.replace(/[^\d]+/g, '');
        if (cpf == '')
            return false;
        // Elimina CPFs invalidos conhecidos    
        if (cpf.length != 11 ||
                cpf == "00000000000" ||
                cpf == "11111111111" ||
                cpf == "22222222222" ||
                cpf == "33333333333" ||
                cpf == "44444444444" ||
                cpf == "55555555555" ||
                cpf == "66666666666" ||
                cpf == "77777777777" ||
                cpf == "88888888888" ||
                cpf == "99999999999")
            return false;
        // Valida 1o digito 
        add = 0;
        for (i = 0; i < 9; i ++)
            add += parseInt(cpf.charAt(i)) * (10 - i);
        rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
            rev = 0;
        if (rev != parseInt(cpf.charAt(9)))
            return false;
        // Valida 2o digito 
        add = 0;
        for (i = 0; i < 10; i ++)
            add += parseInt(cpf.charAt(i)) * (11 - i);
        rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
            rev = 0;
        if (rev != parseInt(cpf.charAt(10)))
            return false;
        return true;
    }

    function verificaEMAIL(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
</script>