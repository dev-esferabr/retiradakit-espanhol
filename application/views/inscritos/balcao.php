<style>
    .existente{
        background-color: #E18B7C;
    }    
    .assinante{
        background: #99a1ff;
    }
    .cortesia{
        background: #b9f1df;
    }
    .cortesia{
        background: #b9f1df;
    }
    .roxo{
        background: #c09ac0;
    }

    .item_pelotao{
        border-radius: 50%;
        border: 1px solid #000;
        padding: 4px 10px;
        margin-left: 10px;
    }

    .pelotao_quenia{
        background: black;
    }
    .pelotao_azul{
        background: blue;
    }

    .pelotao_verde{
        background: green;
    }

    .pelotao_branco{
        background: white;
    }

    .th_camisas{
        padding: 7px 10px;
        border: 1px solid #ddd;
        font-weight: bold;
    }
    .td_camisas{
        padding: 7px 10px;
        border: 1px solid #ddd;        
        text-align: center !important;
    }
</style>
<div class="pageheader">
    <ul class="hornav">
        <li class="current"><a href="#event_new">Pedidos</a></li>

    </ul>
</div><!--pageheader-->

<div id="contentwrapper" class="contentwrapper">
    <form class="stdform" id="form_pedido">
        <div id="event_new" class="subcontent">
            <div class="contenttitle2 nomargintop"  style="border:none;">
                <h2>{NOME_BALCAO} - {QTD_ATLETAS} Inscrições</h2>
            </div>
            <br clear="all" />
            <div class="one_half">
                <div class="contenttitle2">
                    <h3>Quantidade de Camisetas - Tamanho Modalidades </h3>
                </div>
                <table cellpadding="0" cellspacing="0" border="0" class="stdtable">
                    <tbody>
                        <tr>                                
                            {QTD_CAMISAS}
                            <th class="th_camisas">{camiseta} - {modalidade}</th>
                            {/QTD_CAMISAS}
                        </tr>

                        <tr>                                
                            {QTD_CAMISAS}
                            <td class="td_camisas">{total}</td>
                            {/QTD_CAMISAS}
                        </tr>
                    </tbody>
                </table>

                <div class="contenttitle2">
                    <h3>Quantidade de Camisetas - Agrupadas por Tamanho </h3>
                </div>
                <table cellpadding="0" cellspacing="0" border="0" class="stdtable">
                    <tbody>
                        <tr>                                
                            {QTD_CAMISAS_AGRUPADAS}
                            <th class="th_camisas">{camiseta}</th>
                            {/QTD_CAMISAS_AGRUPADAS}
                        </tr>

                        <tr>                                
                            {QTD_CAMISAS_AGRUPADAS}
                            <td class="td_camisas">{total}</td>
                            {/QTD_CAMISAS_AGRUPADAS}
                        </tr>
                    </tbody>
                </table>
            </div>


            <br clear="all" />

            <div class="contenttitle2">
                <h3>Altetas Inscritos</h3>
            </div>
            <table cellpadding="0" cellspacing="0" border="0" class="stdtable atletas" id="dyntable">
                <thead>
                    <tr>                        
                        <th class="head1">Protocolo</th>
                        <th class="head0">Nome</th>
                        <th class="head0">Pelotão</th>
                        <th class="head1">Nº Peito</th>
                        <th class="head0">Modalidade</th>
                        <th class="head1">Categoria</th>
                        <th class="head0">Camiseta</th>
                        <th class="head1">Brinde</th>
                        <th class="head1">Personalização</th>
                        <th class="head0">Retirado</th>
                        <th class="head0">Retirar</th>
                    </tr>
                </thead>
                <tbody>{ATLETAS}
                    <tr data-id_usuario="{cod_usuario}" data-cod_inscritos="{cod_inscritos}" class="{class_item}">
                        <td>{id_pedido}</td>

                        <td>                           
                            <a href="#" class="dados_atleta" data-cod_inscritos="{cod_inscritos}" data-id="{cod_inscritos}">{nome}</a>                            
                        </td>

                        <td><span class="item_pelotao {pelotao}"></span></td>

                        <td>{nm_peito}</td>

                        <td>{modalidade}</td>

                        <td>{categoria}</td>

                        <td>{camiseta}</td>

                        <td>{fl_capitao}</td>

                        <td> <span style="color:red">{personalizacao}</span></td>
                        <td>
                            <a href="#" class="retirado_sim" data-id="{cod_inscritos}">{retirado}</a>
                            {cancelar_retirada}
                        </td>
                        <td>
                            <?php if ($id_pedido_status == 2 || $NIVEL_ADMIN == 1) { ?>
                                <?php if (strlen($ID) > 10) { ?>
                                    <input type="checkbox" name="retirada_incricoes[ok]" checked="checked" disabled="disabled" />
                                    <?php
                                } else {
                                    ?>
                                    <input type="checkbox" name="{info_retirado}" value="1" {retiradoCheck} {disabled} />
                                    <?php
                                }
                            }
                            ?>
                        </td>
                    </tr>{/ATLETAS}
                </tbody>
            </table>
            <a href="#" style="color: blue;" onclick="jQuery('input[name^=retirada_incricoes]').prop('checked', true); return false;">Marcar todos</a> |
            <a href="#" style="color: blue;" onclick="jQuery('input[name^=retirada_incricoes]').prop('checked', false); return false;">Desmarcar todos</a>
            <br clear="all" />

            <br />
            <a href="<?php echo $base_url; ?>inscritos/" class="voltar" style="float: left;">Voltar</a>
            <input type="submit" class="submit radius2" value="Retirar" style="float: right;">
            <?php if ($NIVEL_ADMIN > 0): ?>
                <a href="javascript:void(0);" class="voltar" id="alterar" style="float: right;">Alterar</a>
            <?php endif; ?>

        </div><!--contentwrapper-->
    </form>

    <div id="event_old" class="subcontent">
        <table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="table_inscritos">
            <thead>
                <tr>
                    <th class="head1">Data</th>
                    <th class="head0">Funcionário</th>
                    <th class="head1">Quem Retirou</th>
                    <th class="head0">Telefone</th>
                    <th class="head1">Observação</th>
                    <th class="head0">Ação</th>
                </tr>
            </thead>
            <tbody>{HISTORICO}
                <tr>
                    <td>{dt_alterado}</td>
                    <td>{funcionario}</td>
                    <td>{nome}</td>
                    <td>{telefone}</td>
                    <td><pre>{obs}</pre></td>
                    <td>{acao}</td>
                </tr>{/HISTORICO}
            </tbody>
        </table>
    </div>

    <br clear="all" />

</div><!-- centercontent -->
<style>
    .stdtable td {
        vertical-align: top;
    }
</style>
<div id="retirada">
    <div class="contenttitle2">
        <h3>Retirada do Kit</h3>
    </div>
    <br clear="all" />
    <form action="{base_url}inscritos/retirado/{ID_EVENTO}/{ID}" method="post" class="form_retirada">        
        <br clear="all" />
        <table cellpadding="0" cellspacing="0" class="table invoicefor" style="height: 100%;">
            <tbody>
                <tr>
                    <td width="20%" style="vertical-align: middle;"><strong>Nome:</strong></td>
                    <td width="80%"><span>{COMPRADOR}</span><input type="text" name="nome_retirado" class="nome_retirado" value="{COMPRADOR}" data-inicial="{COMPRADOR}" /></td>
                </tr>
                <tr>
                    <td style="vertical-align: middle;"><strong>Telefone:</strong></td>
                    <td><span>{TELEFONE}</span><input type="text" name="telefone_retirado" class="telefone_retirado" value="{TELEFONE}" data-inicial="{TELEFONE}" /></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;"><strong>Observação:</strong></td>
                    <td><textarea name="obs"></textarea></td>
                </tr>
            </tbody>
        </table>
        <div class="inputs_hidden"></div>
        <br clear="all" />
        <a href="<?php echo $base_url; ?>inscritos/" class="buttonFinish" style="float: left;">Voltar</a>
        <input type="submit" class="submit radius2" value="Retirar" style="float: right;">
    </form>
</div>

<script type="text/javascript" src="{base_url}assets/js/plugins/jquery.mask.js"></script>
<script>
                jQuery(document).ready(function (e) {

                    jQuery(".modalidade").change(function () {
                        modali = jQuery(this).find("option:selected").val();
                        jQuery(this).parents('tr').find(".categoria option").css("display", "none");
                        jQuery(this).parents('tr').find(".categoria .visivel").css('display', 'block');
                        jQuery(this).parents('tr').find(".categoria option").each(function () {
                            if (jQuery(this).data("modalidade") == modali) {
                                jQuery(this).css("display", "block")
                            }
                        });
                        jQuery(this).parents('tr').find(".categoria .visivel").attr('selected', 'selected');
                    });

                    jQuery(".atletas").find('tr').find(".categoria option").each(function () {
                        modali = jQuery(this).parents('tr').find(".modalidade option:selected").val();
                        jQuery(this).parents('tr').find(".categoria option").css("display", "none");
                        jQuery(this).parents('tr').find(".categoria .visivel").css('display', 'block');
                        jQuery(this).parents('tr').find(".categoria option").each(function () {
                            if (jQuery(this).data("modalidade") == modali) {
                                jQuery(this).css("display", "block")
                            }
                        });
                    });

                    jQuery('#alterar').click(function () {
                        var arrayObj = new Array();
                        jQuery(".atletas tbody tr").each(function () {
                            var obj = new Object();
                            obj.cod_inscritos = jQuery(this).data('cod_inscritos');
                            obj.id_usuario = jQuery(this).data('id_usuario');
                            obj.id_modalidade = jQuery(this).find('.modalidade option:selected').val();
                            obj.nm_peito = jQuery(this).find('input[name=nm_peito]').val();
                            obj.id_categoria = jQuery(this).find('.categoria option:selected').val();
                            obj.id_tamanho_camiseta = jQuery(this).find('.id_tamanho_camiseta option:selected').val();
                            arrayObj.push(obj);
                        });
                        var jsonObjs = JSON.stringify(arrayObj);

                        jQuery.ajax({
                            type: "POST",
                            url: '{base_url}inscritos/alterar',
                            data: {'dados': jsonObjs},
                            dataType: 'html',
                            cache: false
                        }).done(function (data) {
                            alert('ok');
                        });

                        return false;
                    });

                    jQuery('#form_pedido').submit(function () {
<?php if (strlen($ID) > 10) { ?>
                            document.location = '{base_url}inscritos';
                            return false;
<?php } ?>
                        // Abrir LightBox
                        jQuery.fancybox({
                            maxWidth: 800,
                            maxHeight: 600,
                            fitToView: false,
                            width: '70%',
                            height: '70%',
                            autoSize: false,
                            closeClick: false,
                            openEffect: 'none',
                            closeEffect: 'none',
                            content: jQuery('#retirada').html()
                        });

                        jQuery('.telefone_retirado').mask("(99) 99999-9999");
                        jQuery('.fancybox-outer .form_retirada').addClass('nao_comprador');
                        jQuery('.fancybox-outer .form_retirada .nome_retirado').val('');
                        jQuery('.fancybox-outer .form_retirada .telefone_retirado').val('');

                        // Adicionar Inputs ao LightBox
                        jQuery('#form_pedido input').each(function (index, element) {
                            if (jQuery(this).attr('type') == 'checkbox') {
                                var html = '<input type="hidden" name="' + jQuery(this).attr('name') + '"';
                                html += (jQuery(this).is(':checked') ? ' value="1"' : ' value="0"') + '>';

                                jQuery('.fancybox-outer .form_retirada .inputs_hidden').append(html);
                            }
                        });

                        return false;
                    });
                    triggerRetiradoSim();


                    jQuery('.cancelar_retirada').click(function () {
                        id_inscricao = jQuery(this).data('id');
                        if (confirm("Tem certeza que deseja cancelar a retirada " + id_inscricao)) {
                            jQuery.ajax({
                                type: "POST",
                                url: '{base_url}inscritos/cancelarretirada',
                                data: {id_pedido_evento: id_inscricao},
                                dataType: 'html',
                                cache: false
                            }).done(function (data) {
                                alert('Retirada cancelada.');
                                location.reload();
                            });

                            return false;
                        }
                    });
                });
</script>
