<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<table border='0'>
    <tr>
        <td><strong>Protocolo</strong></td>
        <td><strong>ID Inscrição</strong></td>
        <td><strong>ID Evento</strong></td>
        <td><strong>Nome Evento</strong></td>
        <td><strong>Data Evento</strong></td>        
        <td><strong>Nome</strong></td>        
        <td><strong>Nº Peito</strong></td>        
        <td><strong>Sexo</strong></td>
        <td><strong>Data Nascimento</strong></td>
        <td><strong>Pelotão</strong></td>
        <td><strong>Modalidade</strong></td>
        <td><strong>Categoria</strong></td>
        <td><strong>CPF</strong></td>
        <td><strong>Tipo Documento</strong></td>
        <td><strong>Tipo Usuário</strong></td>
        <td><strong>Telefone</strong></td>
        <td><strong>Celular</strong></td>
        <td><strong>Email</strong></td>
        <td><strong>Forma Pagamento</strong></td>
        <td><strong>Retirado</strong></td>
    </tr>
    <?php
    foreach ($dados as $value):
        ?>
        <tr>            
            <td><?= $value->id_pedido ?></td>
            <td><?= $value->cod_inscritos ?></td>
            <td><?= $value->id_evento ?></td>
            <td><?= $value->nome_evento ?></td>
            <td><?= $value->data_evento ?></td>            
            <td><?= $value->nome ?></td>
            <td><?= $value->nm_peito ?></td>
            <td><?= $value->sexo ?></td>
            <td><?= $value->nascimento ?></td>
            <td><?= $value->pelotao ?></td>
            <td><?= $value->modalidade ?></td>
            <td><?= $value->categoria ?></td>            
            <td><?= $value->documento ?></td>
            <td><?= $value->tipo_documento ?></td>
            <td><?= $value->tipo_usuario ?></td>
            <td><?= $value->telefone ?></td>
            <td><?= $value->celular ?></td>
            <td><?= strtolower($value->email) ?></td>
            <td><?= $value->form_pagamento ?></td>
            <td><?= $value->retirado ?></td>
        </tr>
    <?php endforeach; ?>
</table>
<?php
// Configurações header para forçar o download
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Content-Type: application/x-msexcel");
header('Content-Disposition: attachment; filename="RetiradaKit.xls"');
header("Content-Description: PHP Generated Data");
exit();
?>