<div class="pageheader">
    <h1 class="pagetitle">Funcionários</h1>
    <span class="pagedesc">Administre a lista de funcionários de Retirada de Kits.</span>

    <ul class="hornav" style="height: 1px;">&nbsp;</ul>
</div><!--pageheader-->

<div id="contentwrapper" class="contentwrapper">
    <div class="subcontent">
        {EDITAR}<?php include(dirname(__FILE__) . '/form.php'); ?>{/EDITAR}
    </div>
    <br clear="all" />
</div><!-- centercontent -->