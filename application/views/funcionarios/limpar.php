<script>
    jQuery(document).ready(function (e) {
        jQuery('.btn_trash').click(function () {
            var senha = prompt("Informe a senha para Limpar a base", "");
            if (senha != '1234567') {
                alert("Senha Inválida!");
                return false;
            }
        });
    });
</script>
<div class="pageheader">
    <h1 class="pagetitle">Limpar base</h1>
    <span class="pagedesc">Limpar toda base para reiniciar una retirada de kits.</span>
</div><!--pageheader-->

<div id="contentwrapper" class="contentwrapper">
    <a href="<?php echo base_url(); ?>funcionarios/limpando" class="btn btn_red btn_trash"><span style="color: #FFF;">Limpar</span></a>