<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Inscritos extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->checkLogin();
        $this->load->model('inscritos_model');

// Avisar qual o menu Current
        $this->menuCurrent['inscritos'] = ' class="current"';
        $this->menuCurrent['home'] = '';

        $this->load->model('login_model');
        $this->login_model->atualizarConfigBanco();
    }

// Index
    public function index() {
        $data = array();
        $data['id_evento_atual'] = $this->getEventoAtual()->id_evento;
        $data['evento_nome'] = $this->getEventoAtual()->nome;

// Pegar Informações para as novas incrições
        $data['MODALIDADE'] = $this->inscritos_model->getModalidade($this->getEventoAtual()->id_evento);
        $data['CATEGORIA'] = $this->inscritos_model->getCategoria($this->getEventoAtual()->id_evento);
        $data['CAMISETA'] = $this->inscritos_model->countQuantidadeCamisetaVendida($this->getEventoAtual()->id_evento);

        $data['ID_EVENTO'] = $this->getEventoAtual()->id_evento;

        $fixUrlEncode = $this->inscritos_model->listBalcaoQtd($this->getEventoAtual()->id_evento);
        $resolvedUrlEncode=[];
        foreach ($fixUrlEncode as $keyToFix => $valueToFix) {
            $valueToFix->url_balcao = str_replace('%2F','/',str_replace('+',' ',urlencode($valueToFix->url_balcao)));
            array_push($resolvedUrlEncode, $valueToFix);
        }
        // $data['LISTA_BALCAO'] = $this->inscritos_model->listBalcaoQtd($this->getEventoAtual()->id_evento);
        $data['LISTA_BALCAO'] = $resolvedUrlEncode;

        $this->load('inscritos/index', $data);
    }

// Index
    public function getList($id) {
// Verificar se tem busca
        $where = array();
        if ($this->input->get('search_tipo') != '' && $this->input->get('search_atleta') != '') {
            switch ($this->input->get('search_tipo')) {
                case '0':
                    $where['id_pedido'] = ((int) $this->input->get('search_atleta'));
                    break;
                case '1':
                    $where['documento'] = $this->input->get('search_atleta');
                    break;
                case '2':
                    $where['nm_peito'] = $this->input->get('search_atleta');
                    break;
                case '3':
                    $where['email'] = $this->input->get('search_atleta');
                    break;
                case '4':
                    $where['celular LIKE'] = '%' . $this->input->get('search_atleta') . '%';
                    break;
                case '5':
                    $where['telefone LIKE'] = '%' . $this->input->get('search_atleta') . '%';
                    break;
                case '6':
                    $where['equipe LIKE'] = '%' . $this->input->get('search_atleta') . '%';
                    break;
                case '7':
                    $where['nome LIKE'] = '%' . $this->input->get('search_atleta') . '%';
                    break;
                case '8':
                    $where['nome_balcao LIKE'] = '%' . $this->input->get('search_atleta') . '%';
                    break;
            }
        }

// Pegar lista
        $data = array();
        $data['aaData'] = array();
        $lista = $this->inscritos_model->getList($id, $where, $this->input->get('sem_numero'));

        foreach ($lista as $name => $value) {


            $boolProduto = false;
            if (!isset($boolProdutoId) || $boolProdutoId <> $value->id_pedido) {
                $boolProdutoId = $value->id_pedido;
                $boolProduto = true;
            }

            $existente = 'normal';

            if ($value->fl_assinante == 3) {
                $existente = 'roxo';
            }

            if ($value->fl_assinante == 2) {
                $existente = 'existente';
            }

            if ($value->fl_assinante == 1) {
                $existente = 'assinante';
            }

            // if criado para maratona pão de açúcar quando existem funcionários
            if ($value->form_pagamento == 'CORTESIA' && $value->fl_balcao == 1 && $value->id_evento == 35018) {
                $existente = 'cortesia';
            }

//$value->retirado <> 'SIM' && ($value->id_pedido_status == 2 || $this->nativesession->userdata('logged_in')->administrador == 1)) ? '<input type="checkbox" name="retirada_incricoes[' . $value->cod_inscritos . ']" data-pedido="' . $value->id_pedido . '" data-comprador="' . $value->id_comprador . '" data-nome_comprador="' . $value->nome_comprador . '" data-telefone_comprador="' . $value->telefone_comprador . '" value="1"/>' ;

            $data['aaData'][] = array('DT_RowId' => 'id_' . $value->id_pedido . '_' . $value->cod_inscritos, 'DT_RowClass' => $existente,
                '0' => $value->id_pedido,
                '1' => ($value->nome == '') ? 'Sem Nome' : '<span style="color: pelotao_' . strtolower($value->pelotao) . '"></span>' . $value->nome,
                '2' => '<span class="item_pelotao pelotao_' . strtolower(str_replace('ê', 'e', $value->pelotao)) . '"></span>',
                '3' => $value->nm_peito,
                '4' => (($value->id_pedido_status == 2) ? 'Pago' : 'Não confirmado'),
                '5' => $value->modalidade,
                '6' => $value->categoria,
                '7' => $value->camiseta,
                '8' => $value->fl_capitao,
                '9' => $value->documento,
                '10' => ($boolProduto) ? '<span style="color: red">' . $value->produtos . '</span>' : '',
                '11' => '<span style="color: red">' . $value->personalizacao . '</span>',
                '12' => (($value->retirado <> 'SIM' && ($value->id_pedido_status == 2 || $this->nativesession->userdata('logged_in')->administrador == 1)) ? '<input type="checkbox" name="retirada_incricoes[' . $value->cod_inscritos . ']" data-pedido="' . $value->id_pedido . '" data-comprador="' . $value->id_comprador . '" data-nome_comprador="' . $value->nome_comprador . '" data-telefone_comprador="' . $value->telefone_comprador . '" value="1"/>' : ' '),
                '13' => (($value->retirado_data) ? '<a href="#" class="retirado_sim" data-id="' . $value->cod_inscritos . '">' . $value->retirado . '</a>' : 'Não'),
                '14' => (($value->retirado_data) ? '<span style="display:none;">' . $value->retirado_data . '</span>' . date('d/m/Y H:i:s', strtotime($value->retirado_data)) : '-'));
        }

        echo json_encode($data);
        exit();
    }

    // Index
    public function detalhe($id_evento, $id) {
        $this->load->model('retirada_model');
        $this->load->model('inscritos_model');

        // Verificar se é uma inscrição nova
        if (strlen($id) > 10) {
            $this->detalheNovo($id_evento, $id);
            return true;
        }

        // Pegar lista de inscritos no pedido
        $dadosPedido = $this->inscritos_model->getListByPedido($id);

        // Pegar dados do comprador
        $comprador = $this->inscritos_model->getUsuarioByID($dadosPedido[0]->id_comprador, $dadosPedido[0]->fl_balcao);

        // Dados do pedido
        $data = array();
        $data['ID'] = $id;
        $data['DT_PEDIDO'] = date('d/m/Y H:i:s', strtotime($dadosPedido[0]->dt_pedido));
        $data['STATUS'] = (($dadosPedido[0]->id_pedido_status == 2) ? 'Pago' : 'No confirmado');
        $data['PAGAMENTO'] = $dadosPedido[0]->form_pagamento;
        $data['COMPRADOR'] = $comprador->nome;
        $data['DOCUMENTO'] = $comprador->documento;
        $data['BALCAO'] = $dadosPedido[0]->fl_balcao;
        $data['NOME_BALCAO'] = ($comprador->nome_balcao) ? ' - Balcão: ' . $comprador->nome_balcao : '';
        $data['TELEFONE'] = $comprador->telefone;
        $data['DT_PAGAMENTO'] = date('d/m/Y H:i:s', strtotime($dadosPedido[0]->dt_pagamento));

        $modalidades = $this->inscritos_model->getModalidade($id_evento);
        $categorias = $this->inscritos_model->getCategoria($id_evento);
        $camisetas = $this->inscritos_model->getCamisetasByEvento($id_evento);

        // Lista de Atletas
        $data['ATLETAS'] = $dadosPedido;

        // Criação de array para listagem dos IDs de inscrição do protocolo
        $idsInscricao = [];

        foreach ($data['ATLETAS'] as $name => $value) {

            $data['ATLETAS'][$name]->retiradoCheck = (($data['ATLETAS'][$name]->retirado == 'SIM') ? '' : '');
            $data['ATLETAS'][$name]->disabled = (($data['ATLETAS'][$name]->retirado == 'SIM') ? ' disabled' : '');
            $data['ATLETAS'][$name]->retirado = $data['ATLETAS'][$name]->retirado;
            $data['ATLETAS'][$name]->info_retirado = (($data['ATLETAS'][$name]->retirado != 'SIM') ? 'retirada_incricoes[' . $value->cod_inscritos . ']' : '');

            $data['id_pedido_status'] = $value->id_pedido_status;
            $data['ATLETAS'][$name]->MODALIDADE = $modalidades;
            $data['ATLETAS'][$name]->CATEGORIA = $categorias;
            $data['ATLETAS'][$name]->CAMISETA = $camisetas;
            $data['ATLETAS'][$name]->fl_capitao = (($value->fl_capitao == 1) ? 'SIM' : 'NÃO');
            $data['ATLETAS'][$name]->nm_peito = (($value->nm_peito == '' && $this->nativesession->userdata('logged_in')->administrador == 1) ? '' : $value->nm_peito);

            $data['ATLETAS'][$name]->nome = ($data['ATLETAS'][$name]->nome == '') ? 'Sem Nome' : $data['ATLETAS'][$name]->nome;

            foreach ($data['ATLETAS'][$name]->MODALIDADE as $key => $mod) {
                if ($mod->cod_modalidade == $value->id_modalidade) {
                    $data['ATLETAS'][$name]->ds_modalidade = $mod->nome;
                }
            }

            foreach ($data['ATLETAS'][$name]->CAMISETA as $key => $cam) {
                if ($cam->id_tamanho_camiseta == $value->id_tamanho_camiseta) {
                    $data['ATLETAS'][$name]->ds_camiseta = $cam->nome;
                }
            }

            $data['ATLETAS'][$name]->ds_categoria = $data['ATLETAS'][$name]->categoria;
            

            // caso o usuário tenha inscrição apenas de participação
            if (!$data['ATLETAS'][$name]->ds_camiseta) {
                $data['ATLETAS'][$name]->ds_camiseta = '<span style="color:red">Sem Camiseta</span>';
            }

            if (!$data['ATLETAS'][$name]->ds_categoria) {
                $data['ATLETAS'][$name]->ds_categoria = '';
            }

            $data['ATLETAS'][$name]->class_item = 'normal';

            if ($value->fl_assinante == 1) {
                $data['ATLETAS'][$name]->class_item = 'assinante';
            }elseif ($value->fl_assinante == 2) {
                $data['ATLETAS'][$name]->class_item = 'existente';
            }elseif ($value->fl_assinante == 3) {
                $data['ATLETAS'][$name]->class_item = 'roxo';
            }

            // if criado para maratona pão de açúcar quando existem funcionários
            if ($value->form_pagamento == 'CORTESIA' && $value->fl_balcao == 1 && $id_evento == 35018) {
                $data['ATLETAS'][$name]->class_item = 'cortesia';
            }

            $data['ATLETAS'][$name]->pelotao = 'pelotao_' . strtolower(str_replace('ê', 'e', $value->pelotao));

            // botão para cancelar a retirada
            if ($data['ATLETAS'][$name]->retirado == 'SIM' && isset($value->subiu) && $value->subiu == 0) {
                $data['ATLETAS'][$name]->cancelar_retirada = '<a href="#" class="cancelar_retirada" style="color:red; font-weight:bold;" data-id="' . $value->cod_inscritos . '">( cancelar )</a>';
            } else {
                $data['ATLETAS'][$name]->cancelar_retirada = '';
            }

            // Criação de array para listar IDs de inscrição para cruzamento de produtos pós inscrição
            array_push($idsInscricao, $data['ATLETAS'][$name]->cod_inscritos);
        }

        // Produtos compradores
        $data['PRODUTOS'] = $this->inscritos_model->getCountProdutosEvento($id_evento, $id);

        foreach ($data['PRODUTOS'] as $name => $value) {
            $data['PRODUTOS'][$name]->retirado = (($data['PRODUTOS'][$name]->retirado > 0) ? 'SIM' : 'NÃO');
            $data['PRODUTOS'][$name]->retiradoCheck = (($data['PRODUTOS'][$name]->retirado == 'SIM') ? ' checked' : '');
        }

        // Produtos Pós Inscrição
        $idsInscricao = implode(',', $idsInscricao);

        // echo '<pre>'; print_r($idsInscricao); exit;

        $data['PRODUTOS_POS'] = $this->inscritos_model->getCountProdutosEventoPosInscricao($id_evento, $idsInscricao);

        foreach ($data['PRODUTOS_POS'] as $name => $value) {
            $data['PRODUTOS_POS'][$name]->retirado = (($data['PRODUTOS_POS'][$name]->retirado > 0) ? 'SIM' : 'NÃO');
            $data['PRODUTOS_POS'][$name]->retiradoCheck = (($data['PRODUTOS_POS'][$name]->retirado == 'SIM') ? ' checked' : '');
        }        

        // Dados Estras
        $data['ID_COMPRADOR'] = $dadosPedido[0]->id_comprador;
        $data['ID_EVENTO'] = $id_evento;

        // Pegar histórico do pedido
        $data['HISTORICO'] = $this->retirada_model->getHistoricoPedido($id);

        $this->load('inscritos/detalhe', $data);
    }

    public function alterarusuario() {
        $params = $_GET;

        /* verificando se é uma troca de usuário */
        if ($params['cod_usuario'] == '') {
            $codUsuario = $params['cod_usuario_anterior'];
        } else {
            $codUsuario = $params['cod_usuario'];
        }

        $validarNumPeito = $this->inscritos_model->getValidarNumeroPeitoEvento($params['id_evento'], $params['nm_peito'], $codUsuario);

        if ($validarNumPeito) {
            $arrRetorno['status'] = 'erro';
            $arrRetorno['info'] = 'O número de peito (' . $params['nm_peito'] . ') já está em uso neste evento';

            echo json_encode($arrRetorno);
            exit(0);
        }

        if ($params['edicaoInscricao'] == 1) {
            $arrRetorno['status'] = 'ok';
            $arrRetorno['info'] = 'Inscrição alterada com sucesso !';

            $dados = array(
                'id_modalidade' => $params['modalidade'],
                'id_categoria' => $params['categoria'],
                'id_tamanho_camiseta' => $params['camiseta'],
                'nm_peito' => $params['nm_peito'],
                'cod_inscritos' => $params['cod_inscritos'],
                'fl_alterado' => 1
            );

            $dadosUsuario = array(
                'equipe' => $params['equipe'],
                'cod_usuario' => $params['cod_usuario']
            );

            $this->inscritos_model->updateInscricao($dados, $dadosUsuario);
        } else {
            $arrRetorno['status'] = 'ok';
            $arrRetorno['info'] = 'Registro inserido com sucesso !';

            $dtnascimento = explode('/', $params['dtnascimento']);

            $dados = array(
                'nome' => $params['nome'],
                'dtnascimento' => $dtnascimento[2] . '-' . $dtnascimento[1] . '-' . $dtnascimento[0],
                'telefone' => $params['telefone'],
                'celular' => $params['celular'],
                'documento' => str_replace(array('.', '-'), '', $params['documento']),
                'genero' => $params['genero'],
                'necessidades' => $params['necessidades'],
                'equipe' => $params['equipe'],
                'fl_alterado' => 1
            );

            if ($params['cod_usuario'] > 0) {
                $this->inscritos_model->updateUsuario($params['cod_usuario'], $dados);
            } else {
                // salvar e-mail para novos usuários, edição não pode mudar o e-mail
                $dados['email'] = $params['email'];
                $this->inscritos_model->addUsuario($params['cod_usuario'], $params['cod_inscritos'], $dados);
            }
        }

        echo json_encode($arrRetorno);
    }

    public function alterar() {
        $params = json_decode($_POST['dados']);

        foreach ($params as $insc) {
            $dados = array(
                'id_modalidade' => $insc->id_modalidade,
                'id_categoria' => $insc->id_categoria,
                'nm_peito' => $insc->nm_peito,
                'id_usuario' => $insc->id_usuario,
                'id_tamanho_camiseta' => $insc->id_tamanho_camiseta,
                'fl_alterado' => 1
            );
            $this->inscritos_model->updateInscritos($insc->cod_inscritos, $dados);
        }

        exit(0);
    }

    public function gerarnrpeito() {

        $id_modalidade = $_POST['id_modalidade'];

        $modalidade = $this->inscritos_model->getUltimoNrPeito($id_modalidade);
        $inscritos_novo = $this->inscritos_model->getUltimoNrPeitoPedido($id_modalidade);
        $inscritos = $this->inscritos_model->getUltimoNrPeitoPedidoInscritos($id_modalidade);
        //$pedidos_existente = $this->inscritos_model->getUltimoNrPeitoPedidoExistente($modalidade->id_evento);

        if (count($inscritos_novo) <= 0) {
            $nr_peito = $modalidade->nr_peito_extra_de;
        } else {
            $nr_peito = $inscritos_novo->nm_peito + 1;
        }

        if ($nr_peito >= $modalidade->nr_peito_extra_ate) {
            $nr_peito = -1;
        }

        if ($inscritos->nm_peito >= $nr_peito) {
            $nr_peito = $inscritos->nm_peito + 1;
        }

        if (isset($_POST['cod_inscritos'])) {
            $dados = array(
                'nm_peito' => $nr_peito,
            );
            $this->inscritos_model->updateInscritos($_POST['cod_inscritos'], $dados);
        }
        echo $nr_peito;
        //print_r($modalidade);
        //print_r($inscritos_novo);

        exit(0);
    }

    // Detalhe de Novas inscricoes
    public function detalheNovo($id_evento, $id) {
        $this->load->model('funcionarios_model');

        // Pegar lista de inscritos no pedido
        $dadosPedido = $this->inscritos_model->getPedidoNovoByID($id_evento, $id);

        // Dados do pedido
        $data = array();
        $data['ID'] = $id;
        $data['DT_PEDIDO'] = date('d/m/Y H:i:s', strtotime($dadosPedido->dt_alterado));
        $data['STATUS'] = 'Pago';
        $data['PAGAMENTO'] = (($dadosPedido->forma_pagamento) ? "Dinheiro" : "Cartão");
        $data['COMPRADOR'] = $dadosPedido->nome_completo;
        $data['TELEFONE'] = $dadosPedido->telefone;
        $data['DOCUMENTO'] = $dadosPedido->v_nr_documento;
        $data['DT_PAGAMENTO'] = date('d/m/Y H:i:s', strtotime($dadosPedido->dt_alterado));
        $data['NOME_BALCAO'] = '';

        // Lista de Atletas
        $modalidades = $this->inscritos_model->getModalidade($id_evento);
        $categorias = $this->inscritos_model->getCategoria($id_evento);
        $camisetas = $this->inscritos_model->getCamisetasByEvento($id_evento);

        // key unica
        $name = 0;

        $data['ATLETAS'][$name]->MODALIDADE = $modalidades;
        $data['ATLETAS'][$name]->CATEGORIA = $categorias;
        $data['ATLETAS'][$name]->CAMISETA = $camisetas;

        $data['ATLETAS'][$name]->nome = $dadosPedido->nome_completo;
        $data['ATLETAS'][$name]->cod_inscritos = $dadosPedido->cod_inscritos_novo;
        $data['ATLETAS'][$name]->nm_peito = $dadosPedido->nm_peito;
        $data['ATLETAS'][$name]->fl_capitao = 'Não';
        $data['ATLETAS'][$name]->retirado = 'Sim';
        $data['ATLETAS'][$name]->retiradoCheck = ' checked';
        $data['ATLETAS'][$name]->disabled = ' disabled';


        foreach ($modalidades as $key => $mod) {
            if ($mod->cod_modalidade == $dadosPedido->modalidade) {
                $data['ATLETAS'][$name]->ds_modalidade = $mod->nome;
            }
        }

        foreach ($camisetas as $key => $cam) {
            if ($cam->id_tamanho_camiseta == $dadosPedido->camiseta) {
                $data['ATLETAS'][$name]->ds_camiseta = $cam->nome;
            }
        }


        foreach ($categorias as $key => $cat) {
            if ($cat->cod_categoria == $dadosPedido->categoria) {
                $data['ATLETAS'][$name]->ds_categoria = $cat->nome;
            }
        }

        // caso o usuário tenha inscrição apenas de participação
        if (!$data['ATLETAS'][$name]->ds_camiseta) {
            $data['ATLETAS'][$name]->ds_camiseta = '<span style="color:red">Sem Camiseta</span>';
        }

        if (!$data['ATLETAS'][$name]->ds_categoria) {
            $data['ATLETAS'][$name]->ds_categoria = '';
        }

        $data['ATLETAS'][$name]->personalizacao = '';

        // Produtos compradores
        $data['PRODUTOS'] = array();

        // Dados Estras
        $data['ID_COMPRADOR'] = -1;
        $data['ID_EVENTO'] = $id_evento;

        // Pegar histórico do pedido
        $data['HISTORICO'] = array(array('dt_alterado' => $dadosPedido->dt_alterado,
                'funcionario' => end($this->funcionarios_model->get($dadosPedido->cod_funcionario))->nome,
                'nome' => $dadosPedido->nome_completo,
                'telefone' => $dadosPedido->telefone,
                'obs' => '',
                'acao' => 'Nova inscrição comprada e retirada.'));

        $this->load('inscritos/detalhe', $data);
    }

    // Marcar como retirado
    public function retirado($id_evento, $id_pedido) {
        $this->load->model('retirada_model');

        // Pegar Post
        $arrInscricoes = $this->input->post('retirada_incricoes');
        $salvar = array();

        /*
          // Salvar lista de pedidos
          foreach ($inscricoes as $name => $value) {
          $aux = intval($this->retirada_model->getRetiradaByIten($name, 0)->retirado);
          if ($value != $aux)
          $salvar[] = array($name, 0, $value);
          }


         *   $produtos = $this->input->post('retirada_produtos');
          // Salvar lista de produtos
          foreach ($produtos as $name => $value) {
          $aux = intval(@$this->retirada_model->getRetiradaByIten(0, $name)->retirado);
          if ($value != $aux)
          $salvar[] = array(0, $name, $value);
          }
         */

        // Salvar dados da retirada
        $idRetiradoInfo = date('Ymdhis') . $id_pedido . rand(0, 3000);
        $data = array();
        $data['eh_comprador'] = ($this->input->post('eh_comprador') == 'on' || $this->input->post('eh_comprador') > 0 ) ? 1 : 0;
        $data['nome'] = $this->input->post('nome_retirado');
        $data['telefone'] = $this->input->post('telefone_retirado');
        $data['obs'] = $this->input->post('obs');
        $data['cod_funcionario'] = $this->nativesession->userdata('logged_in')->cod_funcionario;
        $data['id_pedido'] = $id_pedido;
        $data['cod_retirado_info'] = $idRetiradoInfo;
        $data['dt_alterado'] = date('YmdHis');

        // vai verificar se tem ou não o retirada info
        $this->retirada_model->saveData('retirado_info', $data, array('cod_retirado_info' => $idRetiradoInfo));

        // Salvar Retirada
        foreach ($arrInscricoes as $idInscricao => $value) {
            if ($value == 1) {
                $idCodidoRetirado = date('Ymd') . $idInscricao . $id_pedido . rand(0, 3000);

                $data = array();
                $data['cod_retirado'] = $idCodidoRetirado;
                $data['cod_retirado_info'] = $idRetiradoInfo;
                $data['id_inscritos'] = $idInscricao;
                $data['id_inscritos_produto'] = 0;
                $data['id_evento'] = $id_evento;
                $data['retirado'] = $value;
                $data['dt_alterado'] = date('YmdHis');

                $this->retirada_model->saveData('retirado', $data, array('id_inscritos' => $idInscricao));
            }
        }

        redirect('/inscritos/');
    }

    // Pegar lista de cidades
    public function get_cidade($idEstado) {

        $listaCidade = $this->inscritos_model->getCidade($idEstado);
        echo '<select name="id_cidade" id="id_cidade" required="required">';
        foreach ($listaCidade as $name => $value)
            echo '<option value="' . $value->id_cidade . '">' . $value->ds_cidade . '</option>';
        echo '</select>';
    }

    // Adicionar nova inscrição
    public function nova() {

        $params = $_GET;

        // Pegar dados para salvar nova inscrição
        $data = array();
        $data['id_evento'] = $this->getEventoAtual()->id_evento;
        $data['cod_funcionario'] = $this->nativesession->userdata('logged_in')->cod_funcionario;
        $data['email'] = $params['email'];
        $data['nome_completo'] = $params['nome_completo'];
        $data['nm_peito'] = $params['nm_peito'];
        $data['h_tipo_cpf'] = $params['h_tipo_cpf'];
        $data['v_nr_documento'] = str_replace(array('.', '-'), '', $params['v_nr_documento']);
        $data['nascimento1'] = $params['nascimento1'];
        $data['nascimento2'] = $params['nascimento2'];
        $data['nascimento3'] = $params['nascimento3'];
        $data['genero'] = $params['genero'];
        $data['id_estado'] = $params['id_estado'];
        $data['id_cidade'] = $params['id_cidade'];
        $data['telefone'] = $params['telefone'];
        $data['celular'] = $params['celular'];
        $data['modalidade'] = $params['modalidade'];
        $data['categoria'] = $params['categoria'];
        $data['camiseta'] = $params['camiseta'];
        $data['forma_pagamento'] = $params['forma_pagamento'];
        $data['nm_preco'] = str_replace(array('R$', 'r$', ' '), '', $params['nm_preco']);

        $validarNumPeito = $this->inscritos_model->getValidarNumeroPeitoEvento($data['id_evento'], $data['nm_peito']);

        if ($validarNumPeito) {
            $arrRetorno['status'] = 'erro';
            $arrRetorno['info'] = 'O número de peito (' . $data['nm_peito'] . ') já está em uso neste evento';
        } else {
            // Salvar Nova Inscrição
            $id_pedido = $this->inscritos_model->nova($data);

            // salvando os dados da retirada da inscrição
            // $this->retiradaInscricaoNova($data['id_evento'], $id_pedido);

            $arrRetorno['status'] = 'ok';
            $arrRetorno['info'] = 'Registro inserido com sucesso';
            $arrRetorno['url'] = '/inscritos/detalhe/' . $this->getEventoAtual()->id_evento . '/' . $id_pedido;
        }
        echo json_encode($arrRetorno);
        exit(0);
    }

    // Adicionar inscrição existente
    public function existente() {

        // Pegar dados para salvar nova inscrição
        $data = array();
        $data['id_evento'] = $this->getEventoAtual()->id_evento;
        $data['cod_funcionario'] = $this->nativesession->userdata('logged_in')->cod_funcionario;
        $data['id_pedido'] = $this->input->post('id_pedido');
        $data['email'] = $this->input->post('email');
        $data['nome_completo'] = $this->input->post('nome_completo');
        $data['nm_peito'] = $this->input->post('nm_peito');
        $data['h_tipo_cpf'] = $this->input->post('h_tipo_cpf');
        $data['v_nr_documento'] = str_replace(array('.', '-'), '', $this->input->post('v_nr_documento'));
        $data['nascimento1'] = $this->input->post('nascimento1');
        $data['nascimento2'] = $this->input->post('nascimento2');
        $data['nascimento3'] = $this->input->post('nascimento3');
        $data['genero'] = $this->input->post('genero');
        $data['id_estado'] = $this->input->post('id_estado');
        $data['id_cidade'] = $this->input->post('id_cidade');
        $data['telefone'] = $this->input->post('telefone');
        $data['celular'] = $this->input->post('celular');
        $data['modalidade'] = $this->input->post('modalidade');
        $data['categoria'] = $this->input->post('categoria');
        $data['camiseta'] = $this->input->post('camiseta');
        // $data['forma_pagamento'] = $this->input->post('forma_pagamento');
        // Salvar Nova Inscrição
        $newID = $this->inscritos_model->existente($data);

        /*
          // salvo a retirada
          $this->retiradaInscricaoNova($data['id_evento'], $data['id_pedido']);

          // jogo para a tela de detalhe
          redirect('/inscritos/detalhe/' . $this->getEventoAtual()->id_evento . '/' . $data['id_pedido']);
         */
        redirect('/inscritos');
    }

    // Mostrar dados do atleta
    public function atleta($id, $cod_inscritos) {
        $this->displayTemplate = false;

        // Pegar Dados do Atleta
        // Verificar se é uma inscrição nova
        if (strlen($id) > 10) {
            $data = (array) $this->inscritos_model->getByInscricaoNovoId($id);
            $data['nome'] = $data['nome_completo'];
            $data['documento'] = $data['v_nr_documento'];
            $data['id_modalidade'] = $data['modalidade'];
            $data['id_categoria'] = $data['categoria'];
            $data['id_tamanho_camiseta'] = $data['camiseta'];
            $data['dtnascimento'] = $data['nascimento3'] . '-' . $data['nascimento2'] . '-' . $data['nascimento1'];

            $data['botoes'] = false;
        } else {
            $data = (array) $this->inscritos_model->getByID($id);
            $data['botoes'] = true;
        }

        $data = array_merge((array) $this->inscritos_model->getUsuarioByID($data['id_usuario'], $data['fl_balcao']), $data);

        // pegando os elementos para o select
        $id_evento = $data['id_evento'];
        $modalidades = $this->inscritos_model->getModalidade($id_evento);
        $categorias = $this->inscritos_model->getCategoria($id_evento);
        $camisetas = $this->inscritos_model->getCamisetasByEvento($id_evento);

        $data['arrModalidade'] = $modalidades;
        $data['arrCategorias'] = $categorias;
        $data['arrCamisetas'] = $camisetas;

        // Arrumar dados para a exibição
        $data['dtnascimento'] = date('d/m/Y', strtotime($data['dtnascimento']));
        $data['modalidade'] = $this->inscritos_model->getModalidadeByID($data['id_modalidade'])->nome;
        $data['categoria'] = $this->inscritos_model->getCategoriaByID($data['id_categoria'])->nome;
        $data['camiseta'] = $this->inscritos_model->getCamisetaByID($data['id_tamanho_camiseta'])->nome;
        $data['cod_inscritos'] = $cod_inscritos;
        $data['id_evento'] = $id_evento;


        foreach ($data['arrModalidade'] as $key => $mod) {
            if ($mod->cod_modalidade == $data['id_modalidade']) {
                $data['arrModalidade'][$key]->selected = 'selected';
            } else {
                $data['arrModalidade'][$key]->selected = '';
            }
        }

        foreach ($data['arrCamisetas'] as $key => $cam) {
            if ($cam->id_tamanho_camiseta == $data['id_tamanho_camiseta']) {
                $data['arrCamisetas'][$key]->selected = 'selected';
            } else {
                $data['arrCamisetas'][$key]->selected = '';
            }
        }

        foreach ($data['arrCategorias'] as $key => $cat) {
            if ($cat->cod_categoria == $data['id_categoria']) {
                $data['arrCategorias'][$key]->selected = 'selected';
            } else {
                $data['arrCategorias'][$key]->selected = '';
            }
        }


        $this->load('inscritos/atleta', $data);
    }

    // Mostrar dados do atleta
    public function historico($id_inscritos = 0, $id_produto = 0) {
        $this->displayTemplate = false;
        $this->load->model('retirada_model');

        // Verificar se é uma nova inscricao
        if (strlen($id_inscritos) > 10) {
            $this->historicoNovo($id_inscritos);
            return true;
        }

        // Carregar Histórico do Produto
        $data = (array) $this->retirada_model->getUltimoHistoricoProduto($id_inscritos, $id_produto);
        $data['dt_alterado'] = date('d/m/Y H:i:s', strtotime($data['dt_alterado']));
        $data['obs'] = nl2br($data['obs']);

        $this->load('inscritos/historico', $data);
    }

    // Mostrar dados do atleta
    public function historicoNovo($id_inscritos) {
        // Carregar Histórico do Produto
        $data = (array) $this->inscritos_model->getPedidoNovoByID(0, $id_inscritos);
        $data['dt_alterado'] = date('d/m/Y H:i:s', strtotime($data['dt_alterado']));
        $data['nome'] = $data['nome_completo'];
        $data['obs'] = 'Nova inscrição comprada e retirada';

        $this->load('inscritos/historico', $data);
    }

    // Verificar se já existe algum usuarios com o numero de peito escolhido
    public function nm_peito_repetido($id_evento, $nm_peito) {
        if ($this->inscritos_model->existNmPeito($id_evento, $nm_peito))
            echo 'SIM';
        else
            echo 'NÃO';
        exit();
    }

    // Verificar se já existe algum usuarios com o numero de peito escolhido
    public function pedido_repetido($id_evento, $id_pedido) {
        if ($this->inscritos_model->existPedido($id_evento, $id_pedido))
            echo 'SIM';
        else
            echo 'NÃO';
        exit();
    }

    // Marcar como retirado
    public function retiradaInscricaoNova($id_evento, $id_pedido) {
        $this->load->model('retirada_model');

        // Salvar dados da retirada
        $data = array();
        $data['eh_comprador'] = 1;
        $data['cod_funcionario'] = $this->nativesession->userdata('logged_in')->cod_funcionario;
        $data['id_pedido'] = $id_pedido;
        $cod_retirado_info = $this->retirada_model->saveInfo($data);

        // Salvar Retirada
        $data = array();
        $data['cod_retirado_info'] = $cod_retirado_info;
        $data['id_inscritos'] = $id_pedido;
        $data['id_inscritos_produto'] = 0;
        $data['id_evento'] = $id_evento;
        $data['retirado'] = 1;
        $this->retirada_model->saveNova($data);
    }

    // Marcar como retirado
    public function cancelarretirada() {
        $this->load->model('inscritos_model');
        $idPedidoEvento = $_POST['id_pedido_evento'];

        $this->inscritos_model->cancelarRetirada($idPedidoEvento);
    }

    public function balcao($id_evento, $infoNomeBalcao) {
        $this->load->model('retirada_model');
        $this->load->model('inscritos_model');

        // pegando o valor do _GET
        $nomeBalcao = urldecode($infoNomeBalcao);

        // Pegar lista de inscritos no pedido
        $dadosPedido = $this->inscritos_model->getListByPedidoBalcao($id_evento, $nomeBalcao);

        // Pegar dados do comprador
        $comprador = $this->inscritos_model->getUsuarioByID($dadosPedido[0]->id_comprador, $dadosPedido[0]->fl_balcao);

        // Dados do pedido
        $data = array();
        $data['NOME_BALCAO'] = $nomeBalcao;


        $arrModalidades = $this->inscritos_model->getModalidade($id_evento);
        $arrCategorias = $this->inscritos_model->getCategoria($id_evento);
        $arrCamisetas = $this->inscritos_model->getCamisetasByEvento($id_evento);

        // Lista de Atletas
        $data['ATLETAS'] = $dadosPedido;

        $data['QTD_ATLETAS'] = count($dadosPedido);

        $data['QTD_CAMISAS'] = $this->inscritos_model->countQuantidadeCamisetaPorBalcao($id_evento, $nomeBalcao);

        $data['QTD_CAMISAS_AGRUPADAS'] = $this->inscritos_model->countQuantidadeCamisetaPorBalcaoAgrupadosPorModalidade($id_evento, $nomeBalcao);

        foreach ($data['ATLETAS'] as $name => $value) {


            $data['ATLETAS'][$name]->retiradoCheck = (($data['ATLETAS'][$name]->retirado == 'SIM') ? '' : '');
            $data['ATLETAS'][$name]->disabled = (($data['ATLETAS'][$name]->retirado == 'SIM') ? ' disabled' : '');
            $data['ATLETAS'][$name]->retirado = $data['ATLETAS'][$name]->retirado;
            $data['ATLETAS'][$name]->info_retirado = (($data['ATLETAS'][$name]->retirado != 'SIM') ? 'retirada_incricoes[' . $value->cod_inscritos . ']' : '');

            $data['id_pedido_status'] = $value->id_pedido_status;
            $data['ATLETAS'][$name]->fl_capitao = (($value->fl_capitao == 1) ? 'SIM' : 'NÃO');
            $data['ATLETAS'][$name]->nm_peito = (($value->nm_peito == '' && $this->nativesession->userdata('logged_in')->administrador == 1) ? '' : $value->nm_peito);

            $data['ATLETAS'][$name]->nome = ($data['ATLETAS'][$name]->nome == '') ? 'Sem Nome' : $data['ATLETAS'][$name]->nome;


            // caso o usuário tenha inscrição apenas de participação
            if (!$data['ATLETAS'][$name]->camiseta) {
                $data['ATLETAS'][$name]->camiseta = '<span style="color:red">Sem Camiseta</span>';
            }

            if (!$data['ATLETAS'][$name]->categoria) {
                $data['ATLETAS'][$name]->categoria = '';
            }

            $data['ATLETAS'][$name]->class_item = 'normal';

            if ($value->fl_assinante == 1) {
                $data['ATLETAS'][$name]->class_item = 'assinante';
            }elseif ($value->fl_assinante == 2) {
                $data['ATLETAS'][$name]->class_item = 'existente';
            }elseif ($value->fl_assinante == 3) {
                $data['ATLETAS'][$name]->class_item = 'roxo';
            }

            // if criado para maratona pão de açúcar quando existem funcionários
            if ($value->form_pagamento == 'CORTESIA' && $value->fl_balcao == 1 && $id_evento == 35018) {
                $data['ATLETAS'][$name]->class_item = 'cortesia';
            }

            $data['ATLETAS'][$name]->pelotao = 'pelotao_' . strtolower(str_replace('ê', 'e', $value->pelotao));

            // botão para cancelar a retirada
            if ($data['ATLETAS'][$name]->retirado == 'SIM' && isset($value->subiu) && $value->subiu == 0) {
                $data['ATLETAS'][$name]->cancelar_retirada = '<a href="#" class="cancelar_retirada" style="color:red; font-weight:bold;" data-id="' . $value->cod_inscritos . '">( cancelar )</a>';
            } else {
                $data['ATLETAS'][$name]->cancelar_retirada = '';
            }
        }

        // Produtos compradores
        $data['PRODUTOS'] = $this->inscritos_model->getCountProdutosEvento($id_evento, $id);

        foreach ($data['PRODUTOS'] as $name => $value) {
            $data['PRODUTOS'][$name]->retirado = (($data['PRODUTOS'][$name]->retirado > 0) ? 'SIM' : 'NÃO');
            $data['PRODUTOS'][$name]->retiradoCheck = (($data['PRODUTOS'][$name]->retirado == 'SIM') ? ' checked' : '');
        }

        // Dados Estras
        $data['ID_COMPRADOR'] = $dadosPedido[0]->id_comprador;
        $data['ID_EVENTO'] = $id_evento;

        // Pegar histórico do pedido
        $data['HISTORICO'] = $this->retirada_model->getHistoricoPedido($id);

        $this->load('inscritos/balcao', $data);
    }

}
