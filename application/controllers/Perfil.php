<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Perfil extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->checkLogin();
        $this->load->model('login_model');
        $this->login_model->atualizarConfigBanco();
    }

    // Index
    public function editar() {
        $data = array();
        $this->load('perfil/editar', $data);
    }

}
