<?php

class Inscritos_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function getByID($id) {
        return $this->db->from('inscritos')->where('cod_inscritos', $id)->get()->row();
    }

    public function getByInscricaoNovoId($id) {
        return $this->db->from('inscritos_novo')->where('cod_inscritos_novo', $id)->get()->row();
    }

    public function buscarEmailInscricoesNovas() {
        $resultBanco = $this->db->select(array('nm_peito', 'v_nr_documento'))->from('inscritos_novo')->get()->result();

        if ($resultBanco) {
            foreach ($resultBanco as $info) {
                $arrRetorno[$info->v_nr_documento] = $info->nm_peito;
            }

            return $arrRetorno;
        } else {
            return null;
        }
    }

    public function buscarPedidosExistentes() {
        $resultBanco = $this->db->select(array('id_pedido'))->from('pedidos_existentes')->get()->result();

        if ($resultBanco) {
            foreach ($resultBanco as $info) {
                $arrRetorno[] = $info->id_pedido;
            }

            return $arrRetorno;
        } else {
            return null;
        }
    }

    public function getModalidadeByID($id) {
        return $this->db->from('evento_modalidade')->where('cod_modalidade', $id)->get()->row();
    }

    public function getCategoriaByID($id) {
        return $this->db->from('evento_categoria')->where('cod_categoria', $id)->get()->row();
    }

    public function getCamisetaByID($id) {
        return $this->db->from('tamanho_camiseta')->where('cod_tamanho_camiseta', $id)->get()->row();
    }

    public function getCamisetasByEvento($id) {
        $this->db->select(array('cod_tamanho_camiseta', 'nome', 'id_tamanho_camiseta'));
        $this->db->from('inscritos');
        $this->db->join('tamanho_camiseta', 'id_tamanho_camiseta = cod_tamanho_camiseta');
        $this->db->where('tamanho_camiseta.id_evento', $id);
        $this->db->order_by('id_tamanho_camiseta', 'asc');
        $this->db->group_by('id_tamanho_camiseta');

        return $this->db->get()->result();
    }

    public function countInscricoes($WHERE = array()) {
        // Contar Inscritos
        $this->db->select('count(*) as total');
        $this->db->from('inscritos');
        $this->db->where($WHERE);
        $total = $this->db->get()->row();

        // Contar Produtos
        $this->db->select('count(*) as total');
        $this->db->from('inscritos_produto');
        $this->db->where($WHERE);
        $total2 = $this->db->get()->row();

        // EVERTON - 11/04/2018 return intval($total->total + $total2->total);
        return intval($total->total);
    }

    public function countRetirados($WHERE = array()) {
        $this->db->select('sum(retirado) as total');
        $this->db->from('retirado_unico');
        $this->db->where($WHERE);
        $total = $this->db->get()->row();

        return intval($total->total);
    }

    public function countQuantidadeCamisetaVendida($id) {
        $this->db->select(array('cod_tamanho_camiseta', 'nome', 'count(*) as total'));
        $this->db->from('inscritos');
        $this->db->join('tamanho_camiseta', 'id_tamanho_camiseta = cod_tamanho_camiseta');
        $this->db->where('inscritos.id_evento', $id);
        $this->db->order_by('id_tamanho_camiseta', 'asc');
        $this->db->group_by('id_tamanho_camiseta');

        return $this->db->get()->result();
    }

    public function countQuantidadeCamisetaRetiradas($id) {
        $this->db->select(array('id_tamanho_camiseta', 'nome', 'sum(retirado) as total'));
        $this->db->from('inscritos');
        $this->db->join('tamanho_camiseta', 'id_tamanho_camiseta = cod_tamanho_camiseta');
        $this->db->join('retirado_unico', 'retirado_unico.id_inscritos = inscritos.cod_inscritos');
        $this->db->where('inscritos.id_evento', $id);
        $this->db->order_by('id_tamanho_camiseta', 'asc');
        $this->db->group_by('id_tamanho_camiseta');

        return $this->db->get()->result();
    }

    public function countQuantidadeCamisetaPorModalidade($id) {
        $this->db->select(array('CONCAT(nome, " (", id_modalidade, ")") as nome', 'sum(retirado) as total'));
        $this->db->from('inscritos');
        $this->db->join('evento_modalidade', 'id_modalidade = cod_modalidade');
        $this->db->join('retirado_unico', 'retirado_unico.id_inscritos = inscritos.cod_inscritos');
        $this->db->where('inscritos.id_evento', $id);
        $this->db->order_by('nome', 'asc');
        $this->db->group_by('id_modalidade');

        return $this->db->get()->result();
    }

    public function countQuantidadeCamisetaPorCategoria($id) {
        $this->db->select(array('CONCAT(nome, " (", cod_categoria, ")") as nome', 'sum(retirado) as total'));
        $this->db->from('inscritos');
        $this->db->join('evento_categoria', 'id_categoria = cod_categoria');
        $this->db->join('retirado_unico', 'retirado_unico.id_inscritos = inscritos.cod_inscritos');
        $this->db->where('inscritos.id_evento', $id);
        $this->db->order_by('nome', 'asc');
        $this->db->group_by('cod_categoria');

        return $this->db->get()->result();
    }

    public function countQuantidadeCamisetaPorBalcao($id, $nomeBalcao) {
        $this->db->select(array('tamanho_camiseta.nome as camiseta', 'COUNT(inscritos.cod_inscritos) AS total', 'evento_modalidade.nome as modalidade'));
        $this->db->from('inscritos');
        $this->db->join('usuario', 'usuario.cod_usuario = inscritos.id_usuario');
        $this->db->join('tamanho_camiseta', 'tamanho_camiseta.cod_tamanho_camiseta = inscritos.id_tamanho_camiseta');
        $this->db->join('evento_modalidade', 'evento_modalidade.cod_modalidade = inscritos.id_modalidade');
        $this->db->where('inscritos.id_evento', $id);
        $this->db->where('usuario.nome_balcao', $nomeBalcao);
        $this->db->where('inscritos.fl_entrega', 2);
        $this->db->where('usuario.fl_balcao', 2);
        $this->db->group_by('evento_modalidade.cod_modalidade');
        $this->db->group_by('tamanho_camiseta.nome');
        $this->db->order_by('total', 'desc');

        return $this->db->get()->result();
    }

    public function countQuantidadeCamisetaPorBalcaoAgrupadosPorModalidade($id, $nomeBalcao) {
        $this->db->select(array('tamanho_camiseta.nome as camiseta', 'COUNT(inscritos.cod_inscritos) AS total'));
        $this->db->from('inscritos');
        $this->db->join('usuario', 'usuario.cod_usuario = inscritos.id_usuario');
        $this->db->join('tamanho_camiseta', 'tamanho_camiseta.cod_tamanho_camiseta = inscritos.id_tamanho_camiseta');
        $this->db->join('evento_modalidade', 'evento_modalidade.cod_modalidade = inscritos.id_modalidade');
        $this->db->where('inscritos.id_evento', $id);
        $this->db->where('usuario.nome_balcao', $nomeBalcao);
        $this->db->where('inscritos.fl_entrega', 2);
        $this->db->where('usuario.fl_balcao', 2);
        $this->db->group_by('tamanho_camiseta.nome');
        $this->db->order_by('total', 'desc');

        return $this->db->get()->result();
    }

    public function getList($id, $where = array(), $sem_numero = 0) {

        $this->db->select(array('*'));
        $this->db->from('view_retirada_kit_inscritos');

        if (count($where) > 0)
            $this->db->where($where);

        if ($sem_numero > 0)
            $this->db->where('nm_peito IS NULL');

        return $this->db->get()->result();
    }

    public function getListByPedido($id) {
        $this->db->select(array('*'));
        $this->db->from('view_detalhe_pedido');
        $this->db->where('id_pedido', $id);
        $this->db->order_by('nome', 'asc');

        return $this->db->get()->result();
    }

    public function getCountProdutosEvento($id, $id_pedido = 0) {
        $this->db->select(array('cod_pedido_produto', 'ds_titulo', 'ds_recurso', 'ds_recurso2', 'count(*) as total', 'SUM(retirado) as retirado'));
        $this->db->from('inscritos_produto p');
        $this->db->join('retirado_unico', 'retirado_unico.id_inscritos_produto = p.cod_pedido_produto', 'left');
        $this->db->where('p.id_evento', $id);
        if ($id_pedido > 0)
            $this->db->where('p.id_pedido', $id_pedido);
        $this->db->order_by('ds_titulo', 'asc');
        $this->db->group_by(array('ds_titulo', 'ds_recurso', 'ds_recurso2'));

        return $this->db->get()->result();
    }

    public function getCountProdutosEventoPosInscricao($id, $idsInscricao = 0) {
        $this->db->select(array('cod_pedido_produto', 'ds_titulo', 'ds_recurso', 'ds_recurso2', 'count(*) as total', 'SUM(retirado) as retirado'));
        $this->db->from('inscritos_produto_pos_inscricao p');
        $this->db->join('retirado_unico', 'retirado_unico.id_inscritos_produto = p.cod_pedido_produto', 'left');
        $this->db->where('p.id_pedido_evento IN ('.$idsInscricao.')');
        $this->db->order_by('ds_titulo', 'asc');
        $this->db->group_by(array('ds_titulo', 'ds_recurso', 'ds_recurso2'));

        return $this->db->get()->result();
    }    

    public function getUsuarioByID($id, $fl_balcao = 0) {

        $fl_balcao = ($fl_balcao) ? $fl_balcao : 0;
        $this->db->select('*');
        $this->db->from('usuario');
        $this->db->where('cod_usuario', $id);
        //->where('fl_balcao', $fl_balcao)

        return $this->db->get()->row();
    }

    // Dados para novas inscrições
    public function getCidade($id) {
        $this->db->select('*');
        $this->db->from('sa_cidade');
        $this->db->where('id_estado', $id);

        return $this->db->get()->result();
    }

    public function getModalidade($id_evento) {
        $this->db->select(array('m.cod_modalidade', 'm.nome'));
        $this->db->from('inscritos i');
        $this->db->join('evento_modalidade m', 'i.id_modalidade = m.cod_modalidade');
        $this->db->where('i.id_evento', $id_evento);
        $this->db->group_by('m.cod_modalidade');

        return $this->db->get()->result();
    }

    public function getCategoria($id_evento) {
        $this->db->select(array('c.cod_categoria', 'c.nome', 'i.id_modalidade'));
        $this->db->from('inscritos i');
        $this->db->join('evento_categoria c', 'i.id_categoria = c.cod_categoria');
        $this->db->where('i.id_evento', $id_evento);
        $this->db->group_by('c.cod_categoria');

        return $this->db->get()->result();
    }

    // Salvar Nova Inscrição
    public function nova($data) {
        $data['cod_inscritos_novo'] = date('YmdHis') . $data['id_evento'] . rand(0, 3000);
        $data['dt_alterado'] = date('YmdHis');
        $this->db->insert('inscritos_novo', $data);

        return $data['cod_inscritos_novo'];
    }

    // Salvar Inscrição Existente
    public function existente($data) {
        $data['id_pedido_existente'] = date('YmdHis') . $data['id_evento'] . rand(0, 3000);
        $data['dt_alterado'] = date('YmdHis');
        $this->db->insert('pedidos_existentes', $data);

        return $data['id_pedido_existente'];
    }

    // Pegar pedido novo pelo ID
    public function getPedidoNovoByID($id_evento, $id_inscritos) {
        $this->db->select('*');
        $this->db->from('inscritos_novo');
        if ($id_evento > 0)
            $this->db->where('id_evento', $id_evento);
        $this->db->where('cod_inscritos_novo', $id_inscritos);

        return $this->db->get()->row();
    }

    // Contar Nova Inscrição
    public function countPCNova($id_evento) {
        $this->db->select(array('COUNT(*) as total', 'SUM(subiu) as subiu'));
        $this->db->from('inscritos_novo');
        $this->db->where('id_evento', $id_evento);
        $dados = $this->db->get()->row();

        return array($dados->subiu, $dados->total);
    }

    // Contar Nova Inscrição
    public function countNova($id_evento) {
        $this->db->select(array('count(*) as total'));
        $this->db->from('inscritos_novo');
        $this->db->where('id_evento', $id_evento);

        return $this->db->get()->row()->total;
    }

    // Verificar se numero de peito já está cadastrado para outro atleta
    public function existNmPeito($id_evento, $nm_peito) {
        $numerosEncontrados = 0;

        $this->db->select(array('count(*) as total'));
        $this->db->from('inscritos');
        $this->db->where('id_evento', $id_evento);
        $this->db->where('nm_peito', $nm_peito);
        $numerosEncontrados += $this->db->get()->row()->total;

        $this->db->select(array('count(*) as total'));
        $this->db->from('inscritos_novo');
        $this->db->where('id_evento', $id_evento);
        $this->db->where('nm_peito', $nm_peito);
        $numerosEncontrados += $this->db->get()->row()->total;

        return $numerosEncontrados > 0;
    }

    // Verificar se numero de peito já está cadastrado para outro atleta
    public function existPedido($id_evento, $id_pedido) {
        $numerosEncontrados = 0;

        $this->db->select(array('count(*) as total'));
        $this->db->from('inscritos');
        $this->db->where('id_evento', $id_evento);
        $this->db->where('id_pedido', $id_pedido);
        $numerosEncontrados += $this->db->get()->row()->total;

        return $numerosEncontrados > 0;
    }

    // Pegar Numeros de Peito em Conflito
    public function getPeitoConflito($id_evento) {
        $this->db->select(array('cod_inscritos_novo',
            'nome_completo',
            'nm_peito'));
        $this->db->from('inscritos_novo');
        $this->db->where('conflito_nm_peito', '1');
        $this->db->where('id_evento', $id_evento);

        return $this->db->get()->result();
    }

    // Salvar novo numero de peito para os usuarios novos
    public function saveNmPeito($cod_inscritos_novo, $nm_peito) {
        $this->db->update('inscritos_novo', array('nm_peito' => $nm_peito, 'conflito_nm_peito' => false), array('cod_inscritos_novo' => $cod_inscritos_novo));
    }

    // Alterar inscritos
    public function updateInscritos($cod_inscritos, $data) {
        $this->db->update('inscritos', $data, array('cod_inscritos' => $cod_inscritos));
    }

    // Alterar usuario
    public function addUsuario($cod_usuario, $cod_inscritos, $data) {

        $data['dt_alterado'] = date('Y-m-d H:i:s');
        $data['cod_usuario'] = date('mdHis');
        $data['fl_balcao'] = 1;
        $data['id_pedido_evento'] = $cod_inscritos;
        $usuario = $this->db->insert('usuario', $data);

        $this->db->update('inscritos', array('id_usuario' => $data['cod_usuario']), array('cod_inscritos' => $cod_inscritos));
    }

    // Alterar usuario
    public function updateUsuario($cod_usuario, $data) {
        $this->db->update('usuario', $data, array('cod_usuario' => $cod_usuario));
    }

    // Alterar inscricao
    public function updateInscricao($data, $dataUsuario) {
        $this->db->update('inscritos', $data, array('cod_inscritos' => $data['cod_inscritos']));

        // alterando equipe do usuario        
        $this->db->update('usuario', $dataUsuario, array('cod_usuario' => $dataUsuario['id_usuario']));
    }

    // Get número de peito
    public function getUltimoNrPeito($id_modalidade) {
        $this->db->select(array('*'));
        $this->db->from('evento_modalidade');
        $this->db->where('cod_modalidade', $id_modalidade);
        return $this->db->get()->row();
    }

    // Get número de peito
    public function getUltimoNrPeitoPedido($id_modalidade) {
        $this->db->select(array('nm_peito'));
        $this->db->from('inscritos_novo');
        $this->db->where('modalidade', $id_modalidade);
        $this->db->order_by('cod_inscritos_novo', 'desc');
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    // Get número de peito
    public function getUltimoNrPeitoPedidoInscritos($id_modalidade) {
        $this->db->select(array('nm_peito'));
        $this->db->from('inscritos');
        $this->db->where('id_modalidade', $id_modalidade);
        $this->db->order_by('CAST(nm_peito AS UNSIGNED)', 'desc');
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    // Get número de peito
    public function getUltimoNrPeitoPedidoExistente($id_evento) {
        $this->db->select(array('nm_peito'));
        $this->db->from('pedidos_existentes');
        $this->db->where('id_evento', $id_evento);
        $this->db->order_by('id_pedido_existente', 'desc');
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    // Get validar número de peito
    public function getValidarNumeroPeitoEvento($id_evento, $nm_peito, $cod_usuario = null) {
        // validando com a tabela de inscritos do ativo
        $this->db->select(array('nm_peito', 'id_pedido'));
        $this->db->from('inscritos');
        $this->db->where('nm_peito', $nm_peito);
        $this->db->where('id_evento', $id_evento);
        if ($cod_usuario) {
            $this->db->where('id_usuario <>', $cod_usuario);
        }
        $this->db->limit(1);

        $query1 = $this->db->get_compiled_select();

        // validando com as inscritos_novos
        $this->db->select(array('nm_peito', 'cod_inscritos_novo'));
        $this->db->from('inscritos_novo');
        $this->db->where('nm_peito', $nm_peito);
        $this->db->where('id_evento', $id_evento);
        $this->db->limit(1);

        $query2 = $this->db->get_compiled_select();

        $arrRetorno = $this->db->query('(' . $query1 . ') UNION ALL (' . $query2 . ')')->result();

        if (sizeof($arrRetorno) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getListExport($id) {
        return $this->db->query('SELECT * FROM view_retirada_kit_export ORDER BY id_pedido ASC')->result();
    }

    public function cancelarRetirada($idInscricao) {
        $this->db->delete('retirado', array('id_inscritos' => $idInscricao));
    }

    public function getListByPedidoBalcao($id_evento, $nomeBalcao) {

        $this->db->select(array('*'));
        $this->db->from('view_retirada_kit_inscritos');
        $this->db->where('id_evento', $id_evento);
        $this->db->where('nome_balcao', $nomeBalcao);

        return $this->db->get()->result();
    }

    public function listBalcaoQtd($id) {
        $this->db->select(array('usuario.nome_balcao', 'COUNT(inscritos.cod_inscritos) AS total', 'CONCAT(\'/inscritos/balcao/\',inscritos.id_evento,\'/\',usuario.nome_balcao) AS url_balcao'));
        $this->db->from('inscritos');
        $this->db->join('usuario', 'usuario.cod_usuario = inscritos.id_usuario');
        $this->db->where('inscritos.id_evento', $id);
        $this->db->where('inscritos.fl_entrega', 2);
        $this->db->where('usuario.fl_balcao', 2);
        $this->db->group_by('usuario.nome_balcao');
        $this->db->order_by('usuario.nome_balcao', 'asc');

        return $this->db->get()->result();
    }

}
