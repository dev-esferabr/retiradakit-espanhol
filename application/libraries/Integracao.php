<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Integracao {

    // public $esferabr_ws_url = 'http://webservices2.br/Ativo';
    public $esferabr_ws_url = 'http://webservices.esferabr.com.br/Ativo';
    //  public $esferabr_ws_url_get = 'https://webservices.ativo.com/br';    
    public $esferabr_ws_url_get = 'http://staging.webservices.ativo.com/br';
    public $temInternet = false;

    public function __construct() {
        $CI = &get_instance();
        $this->db = $CI->db;
        // Verificar se a internet está ligada
        $ligada = $this->db->select('internet_ligada')->from('internet_ligada')->limit(1)->get()->row()->internet_ligada;
        $this->temInternet = ($ligada == 'S');
    }

    // Ligar / Desliga Internet
    public function onOff($ligada) {
        $hrUpdate = date('Y-m-d H:i:s', strtotime('+30 seconds'));
        $this->db->update('internet_ligada', array('internet_ligada' => $ligada,
            'dt' => $hrUpdate), array('dt <= ' => date('Y-m-d H:i:s')));
        if ($ligada != 'S')
            $this->temInternet = false;
    }

    // Pegar Próximos eventos do WebService
    public function listNextEvents() {
        if (!$this->temInternet)
            return array();

        return $this->get_ws_request('/retirada/proximos-eventos');
    }

    // Carregar Evento
    public function loadEvents($idEvento, $tipoRetirada = '1,2', $retirada_model, $arrInfoInscritosNovos = null, $arrIdPedidosExistentes = null) {

        // Carregar dados do ativo
        $arrayDadosEvento = $this->get_ws_request('/retirada/carregar-evento/' . $idEvento . '/' . $tipoRetirada);

        if ($this->temInternet === false)
            return 'Falha de conexão ao carregar evento';

        // Array do Evento
        $evento = array();
        $evento['cod_evento'] = $arrayDadosEvento->id_evento;
        $evento['nome'] = $arrayDadosEvento->ds_evento;
        $evento['dtevento'] = $arrayDadosEvento->dt_evento;
        $evento['fl_encerrar_inscricao'] = $arrayDadosEvento->fl_encerrar_inscricao;
        $evento['dt_alterado'] = date('Y-m-d H:i:s');
        $evento['tipo_base'] = $tipoRetirada;

        // Modalidades do evento
        $modalidade = array();
        if ($arrayDadosEvento->modalidades->status == 'ok') {
            foreach ($arrayDadosEvento->modalidades->dados as $name => $value) {
                $aux = array();
                $aux['cod_modalidade'] = $value->id_modalidade;
                $aux['nome'] = $value->nm_modalidade;
                $aux['nr_peito_extra_de'] = $value->nr_peito_extra_de;
                $aux['nr_peito_extra_ate'] = $value->nr_peito_extra_ate;
                $aux['dt_alterado'] = date('Y-m-d H:i:s');
                $aux['id_evento'] = $idEvento;

                $modalidade[] = $aux;
            }
        }

        // Categoria do evento
        $categoria = array();
        if ($arrayDadosEvento->categorias->status == 'ok') {
            foreach ($arrayDadosEvento->categorias->dados as $name => $value) {
                $aux = array();
                $aux['cod_categoria'] = $value->id_categoria;
                $aux['nome'] = $value->nome;
                $aux['descricao'] = $value->ds_kit;
                $aux['dt_alterado'] = date('Y-m-d H:i:s');
                $aux['id_evento'] = $idEvento;

                $categoria[] = $aux;
            }
        }

        if ($this->temInternet === false)
            return 'Falha de conexão ao carregar inscritos';

        $inscritos = array();

        $arrDadosInscritos = $this->get_ws_request('/retirada/inscritos-evento/' . $idEvento . '/' . $tipoRetirada);

        if ($arrDadosInscritos) {
            foreach ($arrDadosInscritos as $name => $value) {
                $aux = array();
                $aux['cod_inscritos'] = $value->id_pedido_evento;
                $aux['id_pedido'] = $value->id_pedido;
                $aux['id_evento'] = $value->id_evento;
                $aux['id_modalidade'] = $value->id_modalidade;
                $aux['id_categoria'] = $value->id_categoria;
                $aux['ds_categoria'] = $value->ds_categoria;
                $aux['id_usuario'] = $value->id_usuario;
                $aux['id_tamanho_camiseta'] = $value->id_tamanho_camiseta;
                $aux['dt_cadastro'] = $value->dt_cadastro;
                $aux['dt_pedido'] = $value->dt_pedido;
                $aux['id_comprador'] = $value->id_comprador;
                $aux['nm_peito'] = $value->nr_peito;
                $aux['id_pedido_status'] = $value->id_pedido_status;
                $aux['form_pagamento'] = $value->form_pagamento;
                $aux['dt_pagamento'] = $value->dt_pagamento;
                $aux['fl_capitao'] = $value->fl_capitao_revezamento;
                $aux['dt_alterado'] = date('Y-m-d H:i:s');
                $aux['fl_balcao'] = $value->fl_local_inscricao;
                $aux['fl_assinante'] = $value->fl_assinante;
                // tipo da entrega
                $aux['fl_entrega'] = $value->tp_entregakit;
                // flag para limpar registros quando uma inscrição é cancelada            
                $aux['fl_cadastro'] = 0;

                $inscritos[] = $aux;

                if (isset($value->retirado)) {
                    $auxRetirada = array();

                    $auxRetirada['retirado'] = $value->retirado;
                    $auxRetirada['id_evento'] = $value->id_evento;
                    $auxRetirada['eh_comprador'] = $value->comprador_retirou;
                    $auxRetirada['nome'] = $value->nome_retirada;
                    $auxRetirada['telefone'] = $value->telefone_retirada;
                    $auxRetirada['obs'] = $value->obs_retirada;
                    $auxRetirada['funcionario'] = $value->id_pedido_retirado_funcionario;
                    $auxRetirada['id_inscricao'] = $value->id_pedido_evento;
                    $auxRetirada['id_pedido'] = $value->id_pedido;
                    $auxRetirada['cod_retirado'] = $value->id_pedido_retirado;
                    $auxRetirada['cod_retirado_info'] = $value->id_pedido_retirado_info;
                    $auxRetirada['dt_retirado'] = $value->dt_retirado;

                    $arrRetirada[$value->id_pedido_evento] = $auxRetirada;
                }

                // removendo as incrições dos pedidos existentes      
                if ($arrIdPedidosExistentes && in_array($value->id_pedido, $arrIdPedidosExistentes)) {
                    $this->delete('pedidos_existentes', array('id_pedido' => $value->id_pedido));
                }
            }
        }

        // Pegar Usuarios do Evento
        if ($this->temInternet === false)
            return 'Falha de conexão ao carregar usuários';

        $usuarios = array();
        $arrDadosUsuarios = $this->get_ws_request('/retirada/usuarios-evento/' . $idEvento . '/' . $tipoRetirada);

        if ($arrDadosUsuarios) {
            foreach ($arrDadosUsuarios as $name => $value) {
                if (in_array($value->ds_email, $arrEmailsInscritosNovos)) {
                    $listEmailInscricoesAtivo[] = $value->ds_email;
                }

                $aux = array();
                $aux['cod_usuario'] = $value->id_usuario;
                $aux['nome'] = $value->ds_nomecompleto;
                $aux['documento'] = $value->nr_documento;
                $aux['dtnascimento'] = $value->dt_nascimento;
                $aux['equipe'] = $value->ds_equipe;
                $aux['email'] = $value->ds_email;
                $aux['telefone'] = $value->nr_telefone;
                $aux['celular'] = $value->nr_celular;
                $aux['necessidades'] = ($value->nm_necessidades_especiais ? $value->nm_necessidades_especiais : 'N');
                $aux['dt_alterado'] = date('Y-m-d H:i:s');
                $aux['fl_balcao'] = $value->fl_local_inscricao;
                $aux['id_tipo_documento'] = $value->tipo_documento;

                /* atualização everton 13-03-2018 */
                $aux['id_pedido_evento'] = $value->id_pedido_evento;
                $aux['genero'] = $value->fl_sexo;
                $aux['pelotao'] = $value->pelotao;
                $aux['personalizacao'] = $value->personalizacao;
                $aux['nome_balcao'] = $value->ds_nomebalcao;
                /* fim atualização */

                $usuarios[] = $aux;
            }
        }

        // Pegar produtos eventos        
        if ($this->temInternet === false)
            return 'Falha de conexão ao carregar produtos';

        $produtos = array();
        if ($arrayDadosEvento->produtos->status == 'ok') {
            foreach ($arrayDadosEvento->produtos->dados as $name => $value) {
                $aux = array();
                $aux['cod_pedido_produto'] = $value->id_pedido_produto;
                $aux['id_pedido'] = $value->id_pedido;
                $aux['id_evento'] = $idEvento;
                $aux['ds_titulo'] = $value->ds_titulo;
                $aux['ds_recurso'] = $value->ds_recurso;
                $aux['ds_recurso2'] = $value->ds_recurso2;
                $aux['dt_alterado'] = date('Y-m-d H:i:s');

                $produtos[] = $aux;
            }
        }

        // Pegar produtos pós inscrição        
        if ($this->temInternet === false)
            return 'Falha de conexão ao carregar produtos pós inscrição';

        $produtosPosInscricao = array();
        if ($arrayDadosEvento->produtosPosInscricao->status == 'ok') {
            foreach ($arrayDadosEvento->produtosPosInscricao->dados as $name => $value) {
                $aux = array();
                $aux['cod_pedido_produto'] = $value->id_pedido_produto;
                $aux['id_pedido_evento'] = $value->id_pedido_evento;
                $aux['id_evento'] = $idEvento;
                $aux['ds_titulo'] = $value->ds_titulo;
                $aux['ds_recurso'] = $value->ds_recurso;
                $aux['ds_recurso2'] = $value->ds_recurso2;
                $aux['dt_alterado'] = date('Y-m-d H:i:s');

                $produtosPosInscricao[] = $aux;
            }
        }        

        if ($this->temInternet === false)
            return 'Falha de conexão ao carregar camisetas';

        // Pegar camisetas do Evento
        $camisetas = array();
        if ($arrayDadosEvento->camisetas->status == 'ok') {
            foreach ($arrayDadosEvento->camisetas->dados as $name => $value) {
                $aux = array();
                $aux['cod_tamanho_camiseta'] = $value->id_tamanho_camiseta;
                $aux['nome'] = $value->ds_tamanho;
                $aux['nome'] = $value->ds_tamanho;
                $aux['id_evento'] = $idEvento;

                $camisetas[] = $aux;
            }
        }

        // Comentado fluxo de pedidos 
        // $this->uploadExistentes($inscritos, $idEvento, $retirada_model);
        // Salvar dados
        $this->save('evento', $evento, array('cod_evento' => $idEvento));

        // setando o evento atual na hora de buscar
        $eventoAtual['cod_atual'] = 1;
        $eventoAtual['id_evento'] = $idEvento;
        $eventoAtual['dt_alterado'] = date('Y-m-d H:i:s');
        $this->save('evento_atual', $eventoAtual, array('id_evento' => $idEvento));


        foreach ($modalidade as $name => $value)
            $this->save('evento_modalidade', $value, array('cod_modalidade' => $value['cod_modalidade']));
        foreach ($categoria as $name => $value)
            $this->save('evento_categoria', $value, array('cod_categoria' => $value['cod_categoria']));
        foreach ($produtos as $name => $value)
            $this->save('inscritos_produto', $value, array('cod_pedido_produto' => $value['cod_pedido_produto']));
        foreach ($produtosPosInscricao as $name => $value)
            $this->save('inscritos_produto_pos_inscricao', $value, array('cod_pedido_produto' => $value['cod_pedido_produto']));
        foreach ($camisetas as $name => $value)
            $this->save('tamanho_camiseta', $value, array('cod_tamanho_camiseta' => $value['cod_tamanho_camiseta']));
        foreach ($usuarios as $name => $value)
            $this->saveUpdate('usuario', $value, array('cod_usuario' => $value['cod_usuario'], 'fl_balcao' => $value['fl_balcao']));
        foreach ($inscritos as $name => $value)
            $this->save('inscritos', $value, array('cod_inscritos' => $value['cod_inscritos']));

        // colocando o fluxo de retirada
        foreach ($arrRetirada as $objRetirada) {
            // inserindo os dados retirada
            $dataRetirado['cod_retirado'] = $objRetirada['cod_retirado'];
            $dataRetirado['cod_retirado_info'] = $objRetirada['cod_retirado_info'];
            $dataRetirado['id_evento'] = $objRetirada['id_evento'];
            $dataRetirado['retirado'] = 1;
            $dataRetirado['subiu'] = 1;
            $dataRetirado['ultimo'] = 1;
            $dataRetirado['id_inscritos'] = $objRetirada['id_inscricao'];
            $dataRetirado['id_inscritos_produto'] = 0;
            $dataRetirado['dt_alterado'] = $objRetirada['dt_retirado'];

            // salvando os dados
            $this->save('retirado', $dataRetirado, array('cod_retirado' => $dataRetirado['cod_retirado']));

            // inserindo os dados retirada info
            $dataRetiradaInfo['cod_retirado_info'] = $objRetirada['cod_retirado_info'];
            $dataRetiradaInfo['eh_comprador'] = $objRetirada['eh_comprador'];
            $dataRetiradaInfo['nome'] = $objRetirada['nome_retirada'];
            $dataRetiradaInfo['telefone'] = $objRetirada['telefone_retirada'];
            $dataRetiradaInfo['obs'] = $objRetirada['obs_retirada'];
            $dataRetiradaInfo['cod_funcionario'] = $objRetirada['funcionario'];
            $dataRetiradaInfo['id_pedido'] = $objRetirada['id_pedido'];
            $dataRetiradaInfo['subiu'] = 1;
            $dataRetiradaInfo['dt_alterado'] = $objRetirada['dt_retirado'];

            // salvando os infos
            if ($dataRetiradaInfo['cod_retirado_info'] != '')
                $this->save('retirado_info', $dataRetiradaInfo, array('cod_retirado_info' => $dataRetiradaInfo['cod_retirado_info']));
        }

        // removendo as inscrições que já estavam na base       
        foreach ($arrInfoInscritosNovos as $key => $value)
            $this->delete('inscritos_novo', array('nm_peito' => $value, 'v_nr_documento =' => $key));


        return true;
    }

    public function uploadExistentes($inscritos, $evento, $retirada_model) {
        if (count($inscritos) > 0) {
            $existentes = $retirada_model->getListExistenteUpload($evento);
            if (count($existentes) > 0) {
                foreach ($existentes as $exis) {
                    foreach ($inscritos as $ins) {
                        if ($ins['id_pedido'] == $exis->id_pedido) {
                            // Salvar dados da retirada
                            $data = array();
                            $data['eh_comprador'] = 1;
                            $data['nome'] = $exis->nome_completo;
                            $data['telefone'] = '';
                            $data['obs'] = 'Aprovado na sincronização';
                            $data['cod_funcionario'] = 1;
                            $data['id_pedido'] = $exis->id_pedido;
                            $cod_retirado_info = $retirada_model->saveInfo($data);

                            $salvar[] = array($ins['cod_inscritos'], 0, 1);

                            // Salvar Retirada
                            foreach ($salvar as $name => $value) {
                                $data = array();
                                $data['cod_retirado_info'] = $cod_retirado_info;
                                $data['id_inscritos'] = $value[0];
                                $data['id_inscritos_produto'] = $value[1];
                                $data['id_evento'] = $evento;
                                $data['retirado'] = $value[2];
                                $retirada_model->save($data);
                            }
                            $retirada_model->saveListExistenteUploadPedido($ins['id_pedido']);
                        }
                    }
                }
            }
        }
    }

    // Subir dados para o servidor
    public function uploadEvent($funcionarios_model, $retirada_model, $id) {


        // envio 3 vezes o total de até 300 itens
        for ($i = 0; $i < 10; $i++) {
            // carregando os dados locais
            $arrFuncionarios = $funcionarios_model->getListUpload();
            $arrRetirada = $retirada_model->getListUpload($id);
            $arrRetiradaInfo = $retirada_model->getListInfoUpload($id);
            $arrInscricoesNovas = $retirada_model->getListNovaUpload($id);
            $arrInscricoesAlteradas = $retirada_model->getListAlteradosUpload($id);
            $arrUsuariosAlterados = $retirada_model->getListUsuariosAlteradosUpload();

//            echo 'funcionários';
//            var_dump($arrFuncionarios);
//            echo 'retirada';
//            var_dump($arrRetirada);
//            echo 'retirada info';
//            var_dump($arrRetiradaInfo);
//            echo 'inscrições nova';
//            var_dump($arrInscricoesNovas);
//            echo 'inscrições alteradas';
//            var_dump($arrInscricoesAlteradas);
//            echo 'usuarios';
//            var_dump($arrUsuariosAlterados);
            // sincronizar as retiradas
            if ($arrRetirada) {
                $returnRetirada = $this->ws_request('/retirada/sincronizar/retiradas', array('id_evento' => $id, 'dados' => $arrRetirada));

                if ($returnRetirada->status == 'ok') {
                    $arrIdRetirdada = array();
                    foreach ($arrRetirada as $value) {
                        $arrIdRetirdada[] = $value->cod_retirado;
                    }
                    $data['retirada'] = $arrIdRetirdada;

                    // Salvar
                    if (count($data['retirada']) > 0)
                        foreach ($data['retirada'] as $value)
                            $retirada_model->saveListUpload($value);
                }
            }

            // sincronizar as retiradas infos
            if ($arrRetiradaInfo) {

                $returnRetiradaInfo = $this->ws_request('/retirada/sincronizar/retiradas-info', array('id_evento' => $id, 'dados' => $arrRetiradaInfo));

                if ($returnRetiradaInfo->status == 'ok') {
                    $arrIdRetirdadaInfo = array();
                    foreach ($arrRetiradaInfo as $value)
                        $arrIdRetirdadaInfo[] = $value->cod_retirado_info;

                    $data['retirada_info'] = $arrIdRetirdadaInfo;

                    // Salvar
                    if (count($data['retirada_info']) > 0)
                        foreach ($data['retirada_info'] as $value)
                            $retirada_model->saveListInfoUpload($value);
                }
            }

            // sincronizar as inscrições alteradas
            if ($arrInscricoesAlteradas) {
                $returnInscricoes = $this->ws_request('/retirada/sincronizar/inscricoes', array('id_evento' => $id, 'dados' => $arrInscricoesAlteradas));

                if ($returnInscricoes->status == 'ok') {
                    $arrInscricoes = array();
                    foreach ($arrInscricoesAlteradas as $value)
                        $arrInscricoes[] = $value->cod_inscritos;

                    $data['inscricoes'] = $arrInscricoes;

                    //Upload
                    if (count($data['inscricoes']) > 0)
                        foreach ($data['inscricoes'] as $value)
                            $retirada_model->saveListAlteradosUpload($value);
                }
            }

            // sincronizar as novas inscrições
            if ($arrInscricoesNovas) {
                $returnNovasInscricoes = $this->ws_request('/retirada/sincronizar/novas-inscricoes', array('id_evento' => $id, 'dados' => $arrInscricoesNovas));

                if ($returnNovasInscricoes->status == 'ok') {
                    $arrIdInscricoesNovas = array();
                    foreach ($arrInscricoesNovas as $value)
                        $arrIdInscricoesNovas[] = $value->cod_inscritos_novo;

                    $data['inscricoes_nova'] = $arrIdInscricoesNovas;

                    // Salvar
                    if (count($data['inscricoes_nova']) > 0)
                        foreach ($data['inscricoes_nova'] as $value)
                            $retirada_model->saveListNovaUpload($value);
                }
            }

            // sincronizar as mudanças de usuários
            if ($arrUsuariosAlterados) {
                $returnUsuarios = $this->ws_request('/retirada/sincronizar/usuarios', array('id_evento' => $id, 'dados' => $arrUsuariosAlterados));

                if ($returnUsuarios->status == 'ok') {
                    $arrUsuarios = array();
                    foreach ($arrUsuariosAlterados as $value)
                        $arrUsuarios[] = $value->cod_usuario;

                    $data['usuarios'] = $arrUsuarios;

                    if (count($data['usuarios']) > 0)
                        $retirada_model->saveListUsuariosAlteradosUpload();
                }
            }

            // sincronizar os funcionários da retirada
            if ($arrFuncionarios) {
                $returnFuncionarios = $this->ws_request('/retirada/sincronizar/funcionarios', array('id_evento' => $id, 'dados' => $arrFuncionarios));

                if ($returnFuncionarios->status == 'ok') {
                    $arrIdFuncionarios = array();
                    foreach ($arrFuncionarios as $value)
                        $arrIdFuncionarios[] = $value->cod_funcionario;

                    $data['funcionarios'] = $arrIdFuncionarios;

                    // Salvar os dados que subiram        
                    if (count($data['funcionarios']) > 0)
                        foreach ($data['funcionarios'] as $value)
                            $funcionarios_model->saveListUpload($value);
                }
            }
        }

        return true;
    }

    // Salvar Dados
    function save($table, $data, $WHERE) {
        /* $this->db->insert($table, $data); */

        $q = $this->db->select('*')->from($table)->where($WHERE)->limit(1)->get();

        // Verificar se esta sendo inserido algum dado nulo
        foreach ($data as $name => $value) {
            if ($value == '')
                unset($data[$name]);
        }

        if ($q->num_rows() > 0)
            $this->db->where($WHERE)->update($table, $data);
        else
            $this->db->insert($table, $data);
    }

    function saveUpdate($table, $data, $WHERE) {
        $q = $this->db->select('*')->from($table)->where($WHERE)->limit(1)->get();

        // Verificar se esta sendo inserido algum dado nulo
        foreach ($data as $name => $value) {
            if ($value == '')
                unset($data[$name]);
        }

        if ($q->num_rows() > 0)
            $this->db->where($WHERE)->update($table, $data);
        else
            $this->db->insert($table, $data);
    }

    // deletar dados
    function delete($table, $WHERE) {
        $this->db->delete($table, $WHERE);
    }

    // Conectar ao WebService do Ativo
    private function ws_request($url, $params = array()) {
        ini_set('max_execution_time', 0);
        set_time_limit(0);

        // Conectar ao Server
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->esferabr_ws_url_get . $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);
        curl_setopt($ch, CURLOPT_POST, count($params) > 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query((array) $params));
        $response = curl_exec($ch);

        //  var_dump($response);
        // Fechar conexão
        curl_close($ch);

        return json_decode($response);
    }

    private function get_ws_request($url, $params = array()) {
        // Conectar ao Server
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->esferabr_ws_url_get . $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);
        $response = curl_exec($ch);

        //  var_dump($response);

        $infoRetorno = json_decode($response);

        // Fechar conexão
        curl_close($ch);

        if ($infoRetorno->status = 'ok') {
            return $infoRetorno->dados;
        } else {
            $this->temInternet = false;
            $response = '[]';
            return json_decode($response);
        }
    }

    /* ATUALIZAÇÂO EVERTON -  04/2018 */

    // atualizando a tabela para comparar em um segundo carregar evento
    public function atualizarInscritosEnvio($idEvento) {
        $this->db->update('inscritos', array('fl_cadastro' => 1), array('id_evento' => $idEvento));
    }

    // removendo as inscrições quando foram eliminadas no ativo.com
    public function excluirInscritosEnvio($idEvento) {
        $this->db->query('DELETE FROM inscritos WHERE fl_cadastro = 1 AND id_evento = ' . $idEvento);
        // $this->db->delete('inscritos', array('fl_cadastro' => 1, 'id_evento' => $idEvento));
    }

}
