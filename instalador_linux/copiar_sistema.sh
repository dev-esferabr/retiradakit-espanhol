#!/bin/bash
echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Preparando o deploy"
export DEBIAN_FRONTEND=noninteractive


echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Atualizando sistema e repositórios"
apt-get update

echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Installing Kit retirada"

echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Git clone"
rm -rf /var/www/
mkdir /var/www
cd /var/www/
git clone -b v2.0 https://thiagossegatto@bitbucket.org/dev-esferabr/retiradakit-espanhol.git RetiradaKIT
chown -R www-data:www-data /var/www
chmod -R 775 /var/www/RetiradaKIT/

echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Vhost"
rm -f /etc/apache2/sites-available/000-default.conf
echo "<VirtualHost *:80>
        ServerAdmin webmaster@localhost

        DocumentRoot /var/www/
        <Directory />
                Options FollowSymLinks MultiViews
    		AllowOverride All
        </Directory>
        <Directory /var/www/>
                Options FollowSymLinks MultiViews
                AllowOverride All
                Order allow,deny
                allow from all
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/error.log
</VirtualHost>
" > /etc/apache2/sites-available/000-default.conf

a2enmod rewrite
service apache2 restart

echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Criação do banco e inserção dos dados"
mysql -u root -parnkm3442342 -e "drop database if exists retirada_kit;";
mysql -u root -parnkm3442342 -e "create database retirada_kit";
mysql -u root -parnkm3442342 retirada_kit < /var/www/RetiradaKIT/database.sql