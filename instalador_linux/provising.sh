#!/bin/bash
echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Preparando o deploy"
export DEBIAN_FRONTEND=noninteractive


echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Atualizando sistema e reposit\u00f3rios"
apt-get update

echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Installing Apache"
apt-get install apache2 -y > /dev/null

echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Installing GIT"
apt-get install git -y > /dev/null

echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Installing Php"
apt install software-properties-common -y > /dev/null
add-apt-repository ppa:ondrej/php -y > /dev/null
apt update -y > /dev/null
apt install php7.4 -y > /dev/null

echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Installing Php Extesions"
sudo apt install php7.4-common php7.4-mysql php7.4-xml php7.4-xmlrpc php7.4-curl php7.4-gd php7.4-imagick php7.4-cli php7.4-dev php7.4-imap php7.4-mbstring php7.4-opcache php7.4-soap php7.4-zip php7.4-intl -y > /dev/null
 > /dev/null

echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Installing Php DebConf"
apt-get install debconf-utils -y > /dev/null

debconf-set-selections <<< "mysql-server mysql-server/root_password password arnkm3442342"
    
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password arnkm3442342"

echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Installing MySQL"
apt-get install mysql-server mysql-client -y > /dev/null
echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Installing Kit retirada"


echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Git clone"
rm -rf /var/www/
mkdir /var/www
cd /var/www/
git clone -b v2.1.3-prod https://thiagossegatto@bitbucket.org/dev-esferabr/retiradakit-espanhol.git RetiradaKIT
chown -R www-data:www-data /var/www
chmod -R 777 /var/www/RetiradaKIT/

echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Vhost"
rm -f /etc/apache2/sites-available/000-default.conf
echo "<VirtualHost *:80>
        ServerAdmin webmaster@localhost

        DocumentRoot /var/www/
        <Directory />
                Options FollowSymLinks MultiViews
    		AllowOverride All
        </Directory>
        <Directory /var/www/>
                Options FollowSymLinks MultiViews
                AllowOverride All
                Order allow,deny
                allow from all
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/error.log
</VirtualHost>
" > /etc/apache2/sites-available/000-default.conf

a2enmod rewrite
service apache2 restart

echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Cria\u00e7\u00e3o do banco e inser\u00e7\u00e3o dos dados"
mysql -u root -parnkm3442342 -e "drop database if exists retirada_kit;";
mysql -u root -parnkm3442342 -e "create database retirada_kit";
mysql -u root -parnkm3442342 retirada_kit < /var/www/RetiradaKIT/database.sql

#echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> criando icone na Home"
#cp /var/www/RetiradaKIT/CentralRetiradaKit.desktop ~/\u00c1rea\ de\ Trabalho/CentralRetiradaKit.desktop
#cp /var/www/RetiradaKIT/RetiradaKit.desktop ~/\u00c1rea\ de\ Trabalho/RetiradaKit.desktop
#chown kit:kit ~/\u00c1rea\ de\ Trabalho/CentralRetiradaKit.desktop
#chown kit:kit ~/\u00c1rea\ de\ Trabalho/RetiradaKit.desktop
#chmod +x ~/\u00c1rea\ de\ Trabalho/CentralRetiradaKit.desktop
#chmod +x ~/\u00c1rea\ de\ Trabalho/RetiradaKit.desktop
